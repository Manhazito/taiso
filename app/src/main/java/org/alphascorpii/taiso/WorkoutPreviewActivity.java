package org.alphascorpii.taiso;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import org.alphascorpii.taiso.adapter.ExerciseItemAdapter;
import org.alphascorpii.taiso.controller.WorkoutController;
import org.alphascorpii.taiso.util.WorkoutFactory;

public class WorkoutPreviewActivity extends Activity {
    @SuppressWarnings("unused")
    private static final String TAG = WorkoutPreviewActivity.class.getCanonicalName();

    private long workoutId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_preview);

        workoutId = getIntent().getLongExtra(WorkoutFactory.WORKOUT_ID, 0);
        WorkoutController workoutController = new WorkoutController(this, null);

        ((Button) findViewById(R.id.startWorkoutButton)).setText(R.string.start_workout);
        if (workoutController.getExercises(this, workoutId).size() < 1) {
            findViewById(R.id.startWorkoutButton).setEnabled(false);
        } else {
            findViewById(R.id.startWorkoutButton).setEnabled(true);
        }

        RecyclerView workoutTypes = (RecyclerView) findViewById(R.id.exerciseList);
        workoutTypes.setLayoutManager(new LinearLayoutManager(this));
        workoutTypes.setItemAnimator(new DefaultItemAnimator());
        workoutTypes.setAdapter(new ExerciseItemAdapter(this, workoutId));
    }

    public void startWorkout(View view) {
        Intent intent = new Intent(getBaseContext(), WorkoutActivity.class);
        intent.putExtra(WorkoutFactory.WORKOUT_ID, workoutId);
        startActivity(intent);
    }
}
