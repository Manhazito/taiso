package org.alphascorpii.taiso.application;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import org.alphascorpii.taiso.model.DaoMaster;
import org.alphascorpii.taiso.model.DaoSession;

/**
 * [description]
 *
 * @author Filipe Ramos
 * @version 1.0
 */
public class TaisoApplication extends Application {
    @SuppressWarnings("unused")
    private static final String TAG = TaisoApplication.class.getSimpleName();

    public static final String SHARED_PREFS_NAME = "com.ramos.filipe.taiso.shared_preferences";

    private DaoSession daoSession;
    private DaoSession userDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        setupPrivateDatabase();
        setupUserDatabase();
    }

    private void setupPrivateDatabase() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "private_db", null);
        SQLiteDatabase db = null;
        try {
            db = helper.getWritableDatabase();
        } catch (SQLiteDatabaseLockedException e) {
            Log.e(TAG, "Database is locked!");
        } catch (SQLiteException e) {
            Log.e(TAG, "Could not access database!");
            e.printStackTrace();
        }

        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
    }

    private void setupUserDatabase() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "user_db", null);
        SQLiteDatabase db = null;
        try {
            db = helper.getWritableDatabase();
        } catch (SQLiteDatabaseLockedException e) {
            Log.e(TAG, "User database is locked!");
        } catch (SQLiteException e) {
            Log.e(TAG, "Could not access user database!");
            e.printStackTrace();
        }

        DaoMaster daoMaster = new DaoMaster(db);
        userDaoSession = daoMaster.newSession();
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

    public DaoSession getUserDaoSession() {
        return userDaoSession;
    }

}
