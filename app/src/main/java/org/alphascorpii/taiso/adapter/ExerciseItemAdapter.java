package org.alphascorpii.taiso.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.alphascorpii.taiso.R;
import org.alphascorpii.taiso.controller.WorkoutController;
import org.alphascorpii.taiso.model.Exercise;

import java.util.ArrayList;

public class ExerciseItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    @SuppressWarnings("unused")
    private static final String TAG = ExerciseItemAdapter.class.getSimpleName();

    private final Context context;
    private ArrayList<Exercise> exercises = new ArrayList<>();
    private WorkoutController workoutController;

    public ExerciseItemAdapter(Context context, long workoutId) {
        this.context = context;

        workoutController = new WorkoutController(context, null);
        exercises = workoutController.getExercises(context, workoutId);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_exercise, parent, false));
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        int baseResource = workoutController.getStringResource(exercises.get(position).getRootExercise().getBase());
        int nameResource = workoutController.getStringResource(exercises.get(position).getRootExercise().getName());
        int imageResource = workoutController.getDrawableResource(exercises.get(position).getRootExercise().getName());
        String setsRepetitions = context.getResources().getString(R.string.sets_repetitions,
                exercises.get(position).getSets(), exercises.get(position).getRepetitions());

        itemViewHolder.exerciseBase.setText(baseResource);
        if (baseResource != nameResource) {
            itemViewHolder.exerciseName.setText(nameResource);
        } else {
            itemViewHolder.exerciseName.setText("");
        }
        itemViewHolder.exerciseRepetitionsDuration.setText(setsRepetitions);
        if (imageResource != 0) {
            itemViewHolder.exerciseImage.setImageDrawable(context.getResources().getDrawable(imageResource));
        } else {
            itemViewHolder.exerciseImage.setImageDrawable(context.getResources().getDrawable(R.drawable.exercises));
        }
    }

    @Override
    public int getItemCount() {
        return exercises.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @SuppressWarnings("unused")
        private final String TAG = ItemViewHolder.class.getSimpleName();

        public final TextView exerciseBase;
        public final TextView exerciseName;
        public final TextView exerciseRepetitionsDuration;
        public final ImageView exerciseImage;

        public ItemViewHolder(View itemView) {
            super(itemView);

            exerciseBase = (TextView) itemView.findViewById(R.id.exerciseBaseName);
            exerciseName = (TextView) itemView.findViewById(R.id.exerciseName);
            exerciseRepetitionsDuration = (TextView) itemView.findViewById(R.id.exerciseRepetitionsDuration);
            exerciseImage = (ImageView) itemView.findViewById(R.id.exerciseImage);
        }
    }
}
