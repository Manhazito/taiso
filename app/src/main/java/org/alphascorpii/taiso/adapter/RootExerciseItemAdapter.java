package org.alphascorpii.taiso.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.alphascorpii.taiso.R;
import org.alphascorpii.taiso.controller.WorkoutController;
import org.alphascorpii.taiso.model.RootExercise;

import java.util.ArrayList;

public class RootExerciseItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    @SuppressWarnings("unused")
    private static final String TAG = RootExerciseItemAdapter.class.getSimpleName();

    private final Context context;
    private ArrayList<RootExercise> rootExercises = new ArrayList<>();
    private WorkoutController workoutController;

    public RootExerciseItemAdapter(Context context, long workoutId) {
        this.context = context;

        workoutController = new WorkoutController(context, null);
        rootExercises = workoutController.getRootExercises(context, workoutId);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_root_exercise, parent, false));
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        int baseResource = workoutController.getStringResource(rootExercises.get(position).getBase());
        int nameResource = workoutController.getStringResource(rootExercises.get(position).getName());
        int imageResource = workoutController.getDrawableResource(rootExercises.get(position).getName());

        itemViewHolder.exerciseBase.setText(baseResource);
        if (baseResource != nameResource) {
            itemViewHolder.exerciseName.setText(nameResource);
        } else {
            itemViewHolder.exerciseName.setText("");
        }
        if (imageResource != 0) {
            itemViewHolder.exerciseImage.setImageDrawable(context.getResources().getDrawable(imageResource));
        } else {
            itemViewHolder.exerciseImage.setImageDrawable(context.getResources().getDrawable(R.drawable.exercises));
        }
    }

    @Override
    public int getItemCount() {
        return rootExercises.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @SuppressWarnings("unused")
        private final String TAG = ItemViewHolder.class.getSimpleName();

        public final TextView exerciseBase;
        public final TextView exerciseName;
        public final ImageView exerciseImage;

        public ItemViewHolder(View itemView) {
            super(itemView);

            exerciseBase = (TextView) itemView.findViewById(R.id.exerciseBaseName);
            exerciseName = (TextView) itemView.findViewById(R.id.exerciseName);
            exerciseImage = (ImageView) itemView.findViewById(R.id.exerciseImage);
        }
    }
}
