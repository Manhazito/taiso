package org.alphascorpii.taiso.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.alphascorpii.taiso.R;
import org.alphascorpii.taiso.controller.WorkoutController;

import java.util.ArrayList;

public class WorkoutGroupItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    @SuppressWarnings("unused")
    private static final String TAG = WorkoutGroupItemAdapter.class.getSimpleName();

    public static final String CUSTOM_WORKOUTS_IDENTIFIER = "custom_workouts";
    public static final String RANDOM_WORKOUTS_IDENTIFIER = "random_workouts";

    private final WorkoutGroupItemListener listener;
    private final Context context;
    private ArrayList<String> workoutGroups = new ArrayList<>();
    private WorkoutController workoutController;

    public WorkoutGroupItemAdapter(Context context, WorkoutGroupItemListener listener) {
        this.listener = listener;
        this.context = context;

        workoutController = new WorkoutController(context, null);
        workoutGroups = workoutController.getWorkoutGroups(context);
        workoutGroups.add(CUSTOM_WORKOUTS_IDENTIFIER);
        workoutGroups.add(RANDOM_WORKOUTS_IDENTIFIER);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_workout_group, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        int nameResource = workoutController.getStringResource(workoutGroups.get(position));
        int imageResource = workoutController.getDrawableResource(workoutGroups.get(position));
        int detailsResource = workoutController.getStringResource(workoutGroups.get(position) + "_details");

        itemViewHolder.workoutGroupIdentifier = workoutGroups.get(position);
        itemViewHolder.workoutGroupName.setText(nameResource);
        itemViewHolder.workoutGroupDetails.setText(detailsResource);
        itemViewHolder.workoutGroupImage.setImageDrawable(context.getResources().getDrawable(imageResource));
    }

    @Override
    public int getItemCount() {
        return workoutGroups.size();
    }

    public interface WorkoutGroupItemListener {
        void onItemClick(String workoutGroupIdentifier);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @SuppressWarnings("unused")
        private final String TAG = ItemViewHolder.class.getSimpleName();

        public String workoutGroupIdentifier;

        public final TextView workoutGroupName;
        public final TextView workoutGroupDetails;
        public final ImageView workoutGroupImage;

        public ItemViewHolder(View itemView) {
            super(itemView);

            workoutGroupName = (TextView) itemView.findViewById(R.id.exerciseBaseName);
            workoutGroupDetails = (TextView) itemView.findViewById(R.id.exerciseName);
            workoutGroupImage = (ImageView) itemView.findViewById(R.id.exerciseImage);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(workoutGroupIdentifier);
        }
    }
}
