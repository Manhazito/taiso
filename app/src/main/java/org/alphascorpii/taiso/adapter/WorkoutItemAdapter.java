package org.alphascorpii.taiso.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.alphascorpii.taiso.controller.WorkoutController;
import org.alphascorpii.taiso.model.Workout;
import org.alphascorpii.taiso.util.Utils;

import java.util.ArrayList;

public class WorkoutItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    @SuppressWarnings("unused")
    private static final String TAG = WorkoutItemAdapter.class.getSimpleName();

    private final WorkoutItemListener listener;
    private final Context context;
    private WorkoutController workoutController;
    private ArrayList<Workout> workoutArrayList = new ArrayList<>();
    private int currentSortDirection = WorkoutController.ORDER_ASC;
    private String workoutIdentifier;

    public WorkoutItemAdapter(Context context, String workoutIdentifier, WorkoutItemListener listener) {
        this.listener = listener;
        this.context = context;

        this.workoutIdentifier = workoutIdentifier;
        workoutController = new WorkoutController(context, null);
        workoutArrayList = workoutController.sortWorkouts(workoutController.getWorkoutsFromGroup(workoutIdentifier), WorkoutController.ORDER_BY_DURATION, currentSortDirection);
    }

    public void sortWorkouts(int sortType) {
        currentSortDirection = (currentSortDirection == WorkoutController.ORDER_ASC) ? WorkoutController.ORDER_DSC : WorkoutController.ORDER_ASC;

        workoutArrayList.clear();
        workoutArrayList = workoutController.sortWorkouts(workoutController.getWorkoutsFromGroup(workoutIdentifier), sortType, currentSortDirection);
        this.notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(org.alphascorpii.taiso.R.layout.item_workout, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        Workout workout = workoutArrayList.get(position);
        int nameResource = workoutController.getStringResource(workout.getName());

        long duration = workoutController.getWorkoutDuration(workout);
        String durationStr = context.getString(org.alphascorpii.taiso.R.string.duration) + ": " + Utils.getDurationTimeHMS(duration, duration);

        itemViewHolder.workoutId = workout.getId();
        itemViewHolder.workoutName.setText(nameResource);
        int workoutDifficultyLevelIconResourceId = org.alphascorpii.taiso.R.drawable.level_1_dark;
        switch (workout.getLevel()) {
            case 2:
                workoutDifficultyLevelIconResourceId = org.alphascorpii.taiso.R.drawable.level_2_dark;
                break;
            case 3:
                workoutDifficultyLevelIconResourceId = org.alphascorpii.taiso.R.drawable.level_3_dark;
                break;
            case 4:
                workoutDifficultyLevelIconResourceId = org.alphascorpii.taiso.R.drawable.level_4_dark;
                break;
            case 5:
                workoutDifficultyLevelIconResourceId = org.alphascorpii.taiso.R.drawable.level_5_dark;
                break;
        }
        itemViewHolder.workoutDifficultyLevelIcon.setImageDrawable(context.getResources().getDrawable(workoutDifficultyLevelIconResourceId));
        itemViewHolder.workoutDuration.setText(durationStr);
    }

    @Override
    public int getItemCount() {
        return workoutArrayList.size();
    }

    public interface WorkoutItemListener {
        void onItemClick(long workoutId);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @SuppressWarnings("unused")
        private final String TAG = ItemViewHolder.class.getSimpleName();

        public long workoutId;

        public final TextView workoutName;
        public final TextView workoutDuration;
        public final ImageView workoutDifficultyLevelIcon;

        public ItemViewHolder(View itemView) {
            super(itemView);

            workoutName = (TextView) itemView.findViewById(org.alphascorpii.taiso.R.id.workoutName);
            workoutDuration = (TextView) itemView.findViewById(org.alphascorpii.taiso.R.id.workoutDuration);
            workoutDifficultyLevelIcon = (ImageView) itemView.findViewById(org.alphascorpii.taiso.R.id.workoutDifficultyLevelIcon);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(workoutId);
        }
    }
}
