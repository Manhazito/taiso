package org.alphascorpii.taiso;

import android.os.Bundle;
import android.app.Activity;

public class WorkoutStatsActivity extends Activity {
    @SuppressWarnings("unused")
    private static final String TAG = WorkoutStatsActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_stats);
    }
}
