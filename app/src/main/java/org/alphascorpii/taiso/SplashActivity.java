package org.alphascorpii.taiso;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import org.alphascorpii.taiso.application.TaisoApplication;
import org.alphascorpii.taiso.controller.WorkoutController;

public class SplashActivity extends Activity implements WorkoutController.JsonLoaderCallback {
    @SuppressWarnings("unused")
    private static final String TAG = SplashActivity.class.getSimpleName();

    private static final int WAIT_TIME = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                WorkoutController workoutController = new WorkoutController(SplashActivity.this, null);
                workoutController.loadJsonWorkouts(SplashActivity.this, ((TaisoApplication) getApplication()).getDaoSession());
            }
        }, WAIT_TIME);
    }

    @Override
    public void finishedLoading() {
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        startActivity(intent);
    }
}
