package org.alphascorpii.taiso;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.alphascorpii.taiso.application.TaisoApplication;
import org.alphascorpii.taiso.controller.WorkoutController;
import org.alphascorpii.taiso.model.DaoSession;
import org.alphascorpii.taiso.model.Exercise;
import org.alphascorpii.taiso.model.Workout;
import org.alphascorpii.taiso.util.Utils;
import org.alphascorpii.taiso.util.WorkoutFactory;

import java.util.ArrayList;
import java.util.Locale;

public class WorkoutActivity extends Activity implements WorkoutController.TimerCallback {
    @SuppressWarnings("unused")
    private static final String TAG = WorkoutActivity.class.getCanonicalName();

    private static final String WORKOUT_PAUSED = "WORKOUT_PAUSED";
    private static final String EXERCISE_TOKEN = "EXERCISE_TOKEN";
    private static final String EXERCISE_TIME_REMAINING = "EXERCISE_TIME_REMAINING";
    private static final String EXERCISE_INDEX = "EXERCISE_INDEX";
    private static final String REPETITION_INDEX = "REPETITION_INDEX";
    private static final String SETS_INDEX = "SETS_INDEX";

    private long buttonLastClickTime = 0L;
    final private long minClickInterval = 500L;

    private View baseContainer;
    private TextView workoutTitleText;
    private TextView workoutDetailsText;
    private TextView currentExerciseBaseText;
    private TextView currentExerciseNameText;
    private TextView previousExerciseBaseText;
    private TextView previousExerciseNameText;
    private TextView nextExerciseBaseText;
    private TextView nextExerciseNameText;
    private TextView counterText;
    private ProgressBar exerciseProgressBar;
    private LinearLayout setsIndicators;
    private ProgressBar workoutProgressBar;
    private TextView messageText;

    private long totalTimeElapsed;
    private long exerciseTimeElapsed;

    //    private WorkoutCtrl workout;
    private WorkoutController workoutController;
    private Workout workout;
    private ArrayList<Exercise> exercises;
    private int repetitionIndex = 0;
    private int setIndex = 0;
    private int exerciseIndex = 0;
    private boolean needsToExerciseTheOtherSide = false;
    private boolean workoutEnded = false;
    private long workoutDuration = 0L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_workout_simplified);

        baseContainer = findViewById(R.id.baseContainer);
        workoutTitleText = (TextView) findViewById(R.id.workoutTitleText);
        workoutDetailsText = (TextView) findViewById(R.id.workoutDetailsText);
        currentExerciseBaseText = (TextView) findViewById(R.id.currentExerciseNameText);
        currentExerciseNameText = (TextView) findViewById(R.id.currentExerciseDescriptionText);
        previousExerciseBaseText = (TextView) findViewById(R.id.previousExerciseNameText);
        previousExerciseNameText = (TextView) findViewById(R.id.previousExerciseDescriptionText);
        nextExerciseBaseText = (TextView) findViewById(R.id.nextExerciseNameText);
        nextExerciseNameText = (TextView) findViewById(R.id.nextExerciseDescriptionText);
        counterText = (TextView) findViewById(R.id.counterText);
        exerciseProgressBar = (ProgressBar) findViewById(R.id.exerciseProgressBar);
        setsIndicators = (LinearLayout) findViewById(R.id.setsIndicators);
        workoutProgressBar = (ProgressBar) findViewById(R.id.workoutProgressBar);
        messageText = (TextView) findViewById(R.id.messageText);

        if (workoutTitleText == null ||
                workoutDetailsText == null) {
            workoutTitleText = new TextView(this);
            workoutDetailsText = new TextView(this);
        }

        long workoutId = getIntent().getLongExtra(WorkoutFactory.WORKOUT_ID, 0L);
        DaoSession session = ((TaisoApplication) getApplicationContext()).getDaoSession();
        workout = session.getWorkoutDao().load(workoutId);
        workoutController = new WorkoutController(this, this);
        exercises = new ArrayList<>(workoutController.getExercises(this, workoutId));
        workoutDuration = workoutController.getWorkoutDuration(workout);
        workoutController.initSounds(this);

        baseContainer.setOnTouchListener(
                new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (SystemClock.elapsedRealtime() - buttonLastClickTime < minClickInterval)
                            return false;
                        buttonLastClickTime = SystemClock.elapsedRealtime();

                        if (workoutController.isWorkoutStarted()) {
                            if (workoutEnded) {
                                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                                startActivity(intent);
                                finish();
                            } else if (workoutController.isWorkoutPaused()) {
                                workoutController.resumeWorkout();
                                messageText.setText(getString(R.string.click_to_pause));
                            } else {
                                workoutController.pauseWorkout();
                                messageText.setText(getString(R.string.click_to_resume));
                            }
                        } else {
                            Log.d(TAG, "Starting Workout!");
                            needsToExerciseTheOtherSide = !exercises.get(0).getRootExercise().getIsSymmetrical();
                            workoutController.startExercise(exercises.get(0));
                            messageText.setText(getString(R.string.click_to_pause));
                            totalTimeElapsed = 0L;
                            workoutProgressBar.setProgress(0);
                        }

                        return false;
                    }
                }
        );

        if (savedInstanceState != null && savedInstanceState.containsKey(WORKOUT_PAUSED) &&
                savedInstanceState.containsKey(EXERCISE_TIME_REMAINING) && savedInstanceState.containsKey(EXERCISE_INDEX) &&
                savedInstanceState.containsKey(REPETITION_INDEX) && savedInstanceState.containsKey(SETS_INDEX)) {
            Log.d(TAG, "Reconstructing Workout!");

            exerciseIndex = savedInstanceState.getInt(EXERCISE_INDEX);
            repetitionIndex = savedInstanceState.getInt(REPETITION_INDEX);
            setIndex = savedInstanceState.getInt(SETS_INDEX);

            workoutController.startTimer(savedInstanceState.getInt(EXERCISE_TOKEN),
                    savedInstanceState.getLong(EXERCISE_TIME_REMAINING));

            if (savedInstanceState.getBoolean(WORKOUT_PAUSED)) {
                workoutController.pauseWorkout();
            }
        }
        initScreen();
    }

    private void initScreen() {
        if (!TextUtils.isEmpty(workout.getTitle())) {
            workoutTitleText.setText(workout.getTitle());
        } else {
            workoutTitleText.setText(workoutController.getStringResource(workout.getName()));
        }

        if (!TextUtils.isEmpty(workout.getDetails())) {
            workoutDetailsText.setText(workout.getDetails());
        } else {
            String details = getString(R.string.level) + ": " + workout.getLevel()
                    + " | " + getString(R.string.duration) + ": " + Utils.getDurationTimeHMS(workoutDuration, workoutDuration);
            workoutDetailsText.setText(details);
        }

        int baseResource = workoutController.getStringResource(exercises.get(0).getRootExercise().getBase());
        int nameResource = workoutController.getStringResource(exercises.get(0).getRootExercise().getName());
        currentExerciseBaseText.setText(baseResource);
        if (baseResource != nameResource) {
            currentExerciseNameText.setText(nameResource);
        } else {
            currentExerciseNameText.setText("");
        }

        previousExerciseBaseText.setText("");
        previousExerciseNameText.setText("");

        if (exercises.size() > 1) {
            baseResource = workoutController.getStringResource(exercises.get(1).getRootExercise().getBase());
            nameResource = workoutController.getStringResource(exercises.get(1).getRootExercise().getName());
            nextExerciseBaseText.setText(baseResource);
            if (baseResource != nameResource) {
                nextExerciseNameText.setText(nameResource);
            } else {
                nextExerciseNameText.setText("");
            }
        } else {
            nextExerciseBaseText.setText("");
            nextExerciseNameText.setText("");
        }

        String countDownStr;
        if (exercises.get(0).getRepetitions() == 1) {
            long duration = exercises.get(0).getDuration();
            countDownStr = Utils.getDurationTime(duration, duration);
        } else {
            countDownStr = String.format(Locale.ENGLISH, "%02d", exercises.get(0).getRepetitions());
        }
        counterText.setText(countDownStr);
        exerciseProgressBar.setProgress(0);
        workoutProgressBar.setProgress(0);
        exerciseIndex = 0;

        setSetsIndicators();

        messageText.setText(getString(R.string.click_to_start));
    }

    @SuppressWarnings("deprecation")
    private void workoutEnded() {
        workoutController.playLongBeep();
        workoutEnded = true;

        messageText.setText(getString(R.string.click_to_go_back));
        baseContainer.setBackgroundColor(getResources().getColor(R.color.blue));
        workoutProgressBar.setVisibility(View.INVISIBLE);
        exerciseProgressBar.setVisibility(View.INVISIBLE);
        hideSetsIndicators();
        counterText.setText(R.string.congratulations);
        currentExerciseBaseText.setText(R.string.workout_finished);
        currentExerciseNameText.setText("");
        previousExerciseBaseText.setText("");
        previousExerciseNameText.setText("");
        nextExerciseBaseText.setText("");
        nextExerciseNameText.setText("");
    }

    @SuppressWarnings("deprecation")
    private void setSetsIndicators() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Resources resources = getResources();
        int pxMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, resources.getDisplayMetrics());
        layoutParams.setMargins(pxMargin, 0, pxMargin, 0);

        setsIndicators.removeAllViews();
        if (exercises.get(exerciseIndex).getSets() == 1) {
            hideSetsIndicators();
        } else {
            for (int i = 0; i <= setIndex; i++) {
                ImageView circle = new ImageView(this);
                circle.setImageDrawable(getResources().getDrawable(R.drawable.circle));
                circle.setLayoutParams(layoutParams);
                setsIndicators.addView(circle);
            }
            for (int i = (setIndex + 1); i < exercises.get(exerciseIndex).getSets(); i++) {
                ImageView circumference = new ImageView(this);
                circumference.setImageDrawable(getResources().getDrawable(R.drawable.circumference));
                circumference.setLayoutParams(layoutParams);
                setsIndicators.addView(circumference);
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void hideSetsIndicators() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Resources resources = getResources();
        int pxMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, resources.getDisplayMetrics());
        layoutParams.setMargins(pxMargin, 0, pxMargin, 0);

        setsIndicators.removeAllViews();
        ImageView invisibleCircle = new ImageView(this);
        invisibleCircle.setImageDrawable(getResources().getDrawable(R.drawable.invisible_circle));
        invisibleCircle.setLayoutParams(layoutParams);
        setsIndicators.addView(invisibleCircle);
    }

    private void setExercisesInfo() {
        int baseResource = workoutController.getStringResource(exercises.get(exerciseIndex).getRootExercise().getBase());
        int nameResource = workoutController.getStringResource(exercises.get(exerciseIndex).getRootExercise().getName());
        currentExerciseBaseText.setText(baseResource);
        if (nameResource != baseResource) {
            currentExerciseNameText.setText(nameResource);
        } else {
            currentExerciseNameText.setText("");
        }

        if ((exerciseIndex + 1) < exercises.size()) {
            baseResource = workoutController.getStringResource(exercises.get(exerciseIndex + 1).getRootExercise().getBase());
            nameResource = workoutController.getStringResource(exercises.get(exerciseIndex + 1).getRootExercise().getName());
            nextExerciseBaseText.setText(baseResource);
            if (nameResource != baseResource) {
                nextExerciseNameText.setText(nameResource);
            } else {
                nextExerciseNameText.setText("");
            }
        } else {
            nextExerciseBaseText.setText("");
            nextExerciseNameText.setText("");
        }

        if (exerciseIndex > 0) {
            baseResource = workoutController.getStringResource(exercises.get(exerciseIndex - 1).getRootExercise().getBase());
            nameResource = workoutController.getStringResource(exercises.get(exerciseIndex - 1).getRootExercise().getName());
            previousExerciseBaseText.setText(baseResource);
            if (nameResource != baseResource) {
                previousExerciseNameText.setText(nameResource);
            } else {
                previousExerciseNameText.setText("");
            }
        } else {
            previousExerciseBaseText.setText("");
            previousExerciseNameText.setText("");
        }
    }

    private void setExerciseSymmetryPauseInfo() {
        currentExerciseBaseText.setText(R.string.switch_sides);
        currentExerciseNameText.setText(workoutController.getStringResource(exercises.get(exerciseIndex).getRootExercise().getBase()));
    }

    private void setExercisePauseInfo() {
        currentExerciseBaseText.setText(R.string.get_ready);
        currentExerciseNameText.setText(workoutController.getStringResource(exercises.get(exerciseIndex).getRootExercise().getBase()));
    }

    private void setWorkoutPauseInfo() {
        currentExerciseBaseText.setText(R.string.breath);
        currentExerciseNameText.setText("");

        int baseResource = workoutController.getStringResource(exercises.get(exerciseIndex).getRootExercise().getBase());
        int nameResource = workoutController.getStringResource(exercises.get(exerciseIndex).getRootExercise().getName());
        previousExerciseBaseText.setText(baseResource);
        if (nameResource != baseResource) {
            previousExerciseNameText.setText(nameResource);
        } else {
            previousExerciseNameText.setText("");
        }

        hideSetsIndicators();
    }

    @Override
    protected void onPause() {
        workoutController.pauseWorkout();
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (workoutController.isWorkoutPaused()) workoutController.resumeWorkout();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        workoutController.releaseSounds();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "Saving Workout!");
        outState.putBoolean(WORKOUT_PAUSED, workoutController.isWorkoutPaused());
        outState.putLong(EXERCISE_TIME_REMAINING, workoutController.getMillisUntilFinished());
        outState.putInt(EXERCISE_INDEX, exerciseIndex);
        outState.putInt(REPETITION_INDEX, repetitionIndex);
        outState.putInt(SETS_INDEX, setIndex);

        super.onSaveInstanceState(outState);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void timerStarted(int token) {
        if (token == WorkoutController.EXERCISE_TOKEN) {
            workoutController.playShortBeep();

            exerciseTimeElapsed = 0L;
            exerciseProgressBar.setProgress(0);

            setExercisesInfo();
            setSetsIndicators();

            baseContainer.setBackgroundColor(getResources().getColor(R.color.red));
        } else if (token == WorkoutController.EXERCISE_SYMMETRY_PAUSE_TOKEN) {
            workoutController.playShortBeepBeep();

            exerciseTimeElapsed = 0L;

            setExerciseSymmetryPauseInfo();
            baseContainer.setBackgroundColor(getResources().getColor(R.color.orange));
        } else if (token == WorkoutController.EXERCISE_PAUSE_TOKEN) {
            workoutController.playShortBeepBeep();

            exerciseTimeElapsed = 0L;

            setExercisePauseInfo();
            baseContainer.setBackgroundColor(getResources().getColor(R.color.orange));
        } else if (token == WorkoutController.WORKOUT_PAUSE_TOKEN) {
            workoutController.playShortBeepBeepBeep();

            exerciseTimeElapsed = 0L;

            setWorkoutPauseInfo();
            baseContainer.setBackgroundColor(getResources().getColor(R.color.green));
        }
    }

    @Override
    public void timerTicked(int token, long millisUntilFinished, long millisTotal) {
        totalTimeElapsed += (millisTotal - millisUntilFinished) - exerciseTimeElapsed;
        exerciseTimeElapsed = millisTotal - millisUntilFinished;
        long exerciseProgress = 0L;
        long workoutProgress = 0;
        if (millisTotal != 0) {
            exerciseProgress = exerciseProgressBar.getMax() * exerciseTimeElapsed / millisTotal;
        }
        if (workoutDuration != 0) {
            workoutProgress = workoutProgressBar.getMax() * totalTimeElapsed / workoutDuration;
        }
        String countDownStr;
        if (token == WorkoutController.EXERCISE_TOKEN && exercises.get(exerciseIndex).getRepetitions() > 1) {
            countDownStr = String.format(Locale.ENGLISH, "%d", exercises.get(exerciseIndex).getRepetitions() - (repetitionIndex));
        } else {
            countDownStr = Utils.getDurationTime(millisUntilFinished, millisTotal);
        }
        exerciseProgressBar.setProgress((int) exerciseProgress);
        workoutProgressBar.setProgress((int) workoutProgress);
        counterText.setText(countDownStr);
    }

    @Override
    public void timerEnded(int token) {
        if (token == WorkoutController.EXERCISE_TOKEN) {
            // Check if needs to repeat
            if ((repetitionIndex + 1) >= exercises.get(exerciseIndex).getRepetitions()) {
                // Check if it's the last set
                if ((setIndex + 1) >= exercises.get(exerciseIndex).getSets()) {
                    // Check if needs to repeat exercise
                    if (!needsToExerciseTheOtherSide) {
                        // Check if its the last one
                        if ((exerciseIndex + 1) >= exercises.size()) {
                            workoutEnded();
                        } else {
                            workoutController.startWorkoutPause(workout);
                        }
                    } else {
                        workoutController.startExerciseSymmetryPause(exercises.get(exerciseIndex));
                    }
                } else {
                    workoutController.startExercisePause(exercises.get(exerciseIndex));
                }
            } else {
                repetitionIndex++;
                workoutController.startExercise(exercises.get(exerciseIndex));
            }
        } else if (token == WorkoutController.EXERCISE_SYMMETRY_PAUSE_TOKEN) {
            needsToExerciseTheOtherSide = false;
            repetitionIndex = 0;
            setIndex = 0;
            workoutController.startExercise(exercises.get(exerciseIndex));
        } else if (token == WorkoutController.EXERCISE_PAUSE_TOKEN) {
            // Prepare next set!
            setIndex++;
            repetitionIndex = 0;
            workoutController.startExercise(exercises.get(exerciseIndex));
        } else if (token == WorkoutController.WORKOUT_PAUSE_TOKEN) {
            // Prepare next exercise!
            exerciseIndex++;
            setIndex = 0;
            repetitionIndex = 0;
            needsToExerciseTheOtherSide = !exercises.get(exerciseIndex).getRootExercise().getIsSymmetrical();
            workoutController.startExercise(exercises.get(exerciseIndex));
        }
    }
}
