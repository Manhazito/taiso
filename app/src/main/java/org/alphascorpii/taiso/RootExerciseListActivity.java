package org.alphascorpii.taiso;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.alphascorpii.taiso.adapter.RootExerciseItemAdapter;
import org.alphascorpii.taiso.controller.WorkoutController;

public class RootExerciseListActivity extends Activity {
    @SuppressWarnings("unused")
    private static final String TAG = RootExerciseListActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root_exercise_list);

        RecyclerView workoutTypes = (RecyclerView) findViewById(R.id.exerciseList);
        workoutTypes.setLayoutManager(new LinearLayoutManager(this));
        workoutTypes.setItemAnimator(new DefaultItemAnimator());
        workoutTypes.setAdapter(new RootExerciseItemAdapter(this, WorkoutController.ALL_EXERCISES));
    }
}
