package org.alphascorpii.taiso.util;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * [description]
 *
 * @author Filipe Ramos
 * @version 1.0
 */
public class Utils {
    @SuppressWarnings("unused")
    private static final String TAG = Utils.class.getSimpleName();

    static public String getDurationTime(long millis, long millisMax) {
        String timerStr;

        long hoursTotal = TimeUnit.MILLISECONDS.toHours(millisMax);
        long minutesTotal = TimeUnit.MILLISECONDS.toMinutes(millisMax) % TimeUnit.HOURS.toMinutes(1);
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1);
        long centiseconds = (TimeUnit.MILLISECONDS.toMillis(millis) % TimeUnit.SECONDS.toMillis(1))/10L;

        if (hoursTotal != 0L) {
            timerStr = String.format(Locale.ENGLISH, "%02d:%02d:%02d:%02d", hours, minutes, seconds, centiseconds);
        } else if (minutesTotal != 0L) {
            timerStr = String.format(Locale.ENGLISH, "%02d:%02d:%02d", minutes, seconds, centiseconds);
        } else {
            timerStr = String.format(Locale.ENGLISH, "%02d:%02d", seconds, centiseconds);
        }

        return timerStr;
    }

    static public String getDurationTimeHMS(long millis, long millisMax) {
        String timerStr;

        long hoursTotal = TimeUnit.MILLISECONDS.toHours(millisMax);
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1);

        if (hoursTotal != 0L) {
            timerStr = String.format(Locale.ENGLISH, "%02dh%02dm%02ds", hours, minutes, seconds);
        } else {
            timerStr = String.format(Locale.ENGLISH, "%02dm%02ds", minutes, seconds);
        }

        return timerStr;
    }
}
