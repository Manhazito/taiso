package org.alphascorpii.taiso.util;

/**
 * [description]
 *
 * @author Filipe Ramos
 * @version 1.0
 */
public class WorkoutFactory {
    @SuppressWarnings("unused")
    private static final String TAG = WorkoutFactory.class.getSimpleName();

    public final static String WORKOUT_ID = "workoutId";
    public final static String WORKOUT_GROUP_ID = "workoutGroupId";

    // WORKOUTS
    public final static int LUMBAR_STABILIZATION_PROTOCOL_1 = 0;
    public final static int LUMBAR_STABILIZATION_PROTOCOL_2 = 1;
    public final static int LUMBAR_STABILIZATION_PROTOCOL_3 = 2;
    public final static int LUMBAR_STABILIZATION_PROTOCOL_4 = 3;
    public final static int LUMBAR_STABILIZATION_PROTOCOL_5 = 4;
    public final static int HIGH_INTENSITY_INTERVAL_TRAINING_1 = 5;
    public final static int HIGH_INTENSITY_INTERVAL_TRAINING_2 = 6;
    public final static int HIGH_INTENSITY_INTERVAL_TRAINING_3 = 7;
    public final static int HIGH_INTENSITY_INTERVAL_TRAINING_4 = 8;

//
//    static public ArrayList<SetCtrl> getWorkoutSets(Context context, WorkoutCtrl workout, int workoutId) {
//        switch (workoutId) {
//            case LUMBAR_STABILIZATION_PROTOCOL_1:
//                return getLumbarStabilizationProtocolLevel1(context, workout);
//            case LUMBAR_STABILIZATION_PROTOCOL_2:
//                return getLumbarStabilizationProtocolLevel2(context, workout);
//            case LUMBAR_STABILIZATION_PROTOCOL_3:
//                return getLumbarStabilizationProtocolLevel3(context, workout);
//            case LUMBAR_STABILIZATION_PROTOCOL_4:
//                return getLumbarStabilizationProtocolLevel4(context, workout);
//            case LUMBAR_STABILIZATION_PROTOCOL_5:
//                return getLumbarStabilizationProtocolLevel5(context, workout);
//            case HIGH_INTENSITY_INTERVAL_TRAINING_1:
//                return getHighIntensityIntervalTraining10min(context, workout);
//            case HIGH_INTENSITY_INTERVAL_TRAINING_2:
//                return getHighIntensityIntervalTraining20min(context, workout);
//            case HIGH_INTENSITY_INTERVAL_TRAINING_3:
//                return getHighIntensityIntervalTraining30min(context, workout);
//            case HIGH_INTENSITY_INTERVAL_TRAINING_4:
//                return getHighIntensityIntervalTraining8min(context, workout);
//            default:
//                return null;
//        }
//    }
//
//    static public String getWorkoutName(Context context, int workoutId) {
//        switch (workoutId) {
//            case LUMBAR_STABILIZATION_PROTOCOL_1:
//            case LUMBAR_STABILIZATION_PROTOCOL_2:
//            case LUMBAR_STABILIZATION_PROTOCOL_3:
//            case LUMBAR_STABILIZATION_PROTOCOL_4:
//            case LUMBAR_STABILIZATION_PROTOCOL_5:
//                return context.getString(R.string.lumbar_stabilization_protocol);
//            case HIGH_INTENSITY_INTERVAL_TRAINING_1:
//            case HIGH_INTENSITY_INTERVAL_TRAINING_2:
//            case HIGH_INTENSITY_INTERVAL_TRAINING_3:
//            case HIGH_INTENSITY_INTERVAL_TRAINING_4:
//                return context.getString(R.string.high_intensity_interval_training);
//            default:
//                return context.getString(R.string.unnamed);
//        }
//    }
//
//    static public int getWorkoutDifficultyLevel(int workoutId) {
//        switch (workoutId) {
//            case LUMBAR_STABILIZATION_PROTOCOL_1:
//            case HIGH_INTENSITY_INTERVAL_TRAINING_1:
//                return 1;
//            case LUMBAR_STABILIZATION_PROTOCOL_2:
//            case HIGH_INTENSITY_INTERVAL_TRAINING_2:
//                return 2;
//            case LUMBAR_STABILIZATION_PROTOCOL_3:
//            case HIGH_INTENSITY_INTERVAL_TRAINING_3:
//                return 3;
//            case LUMBAR_STABILIZATION_PROTOCOL_4:
//            case HIGH_INTENSITY_INTERVAL_TRAINING_4:
//                return 4;
//            case LUMBAR_STABILIZATION_PROTOCOL_5:
//                return 5;
//            default:
//                return 1;
//        }
//    }
//
//    static private ArrayList<SetCtrl> getLumbarStabilizationProtocolLevel1(Context context, WorkoutCtrl workout) {
//        ArrayList<SetCtrl> allSets = new ArrayList<>();
//
//        ExerciseCtrl pauseExercise;
//        SequenceCtrl pauseSequence;
//        SetCtrl pauseSet;
//
//        ExerciseCtrl partialSitUps;
//        SequenceCtrl partialSitUpsSequence;
//        SetCtrl partialSitUpsSet;
//        ExerciseCtrl diagonalPartialSitUps;
//        SequenceCtrl diagonalPartialSitIpsSequence;
//        SetCtrl diagonalPartialSitUpsSet;
//        ExerciseCtrl dyingBug;
//        SequenceCtrl dyingBugSequence;
//        SetCtrl dyingBugSet;
//        ExerciseCtrl bridge;
//        SequenceCtrl bridgeSequence;
//        SetCtrl bridgeSet;
//        ExerciseCtrl superman;
//        SequenceCtrl supermanSequence;
//        SetCtrl supermanSet;
//        ExerciseCtrl quadruped;
//        SequenceCtrl quadrupedSequence;
//        SetCtrl quadrupedSet;
//        ExerciseCtrl wallSquat;
//        SequenceCtrl wallSquatSequence;
//        SetCtrl wallSquatSet;
//        ExerciseCtrl lunges;
//        SequenceCtrl lungesSequence;
//        SetCtrl lungesSet;
//        ExerciseCtrl pronePlank;
//        SequenceCtrl pronePlankSequence;
//        SetCtrl pronePlankSet;
//
//        pauseExercise = new ExerciseCtrl(context.getString(R.string.rest), 30000, false);
//        pauseSequence = new SequenceCtrl(false, pauseExercise);
//        pauseExercise.setRestInterval(true);
//        pauseSet = new SetCtrl(pauseSequence, 1, true);
//        pauseSet.setCallback(workout);
//        pauseExercise.setCallback(pauseSequence);
//        pauseSequence.setCallback(pauseSet);
//
//        partialSitUps = new ExerciseCtrl(context.getString(R.string.partial_sit_ups), 2500, true);
//        partialSitUps.setImageResource(R.drawable.partial_sit_ups);
//        partialSitUpsSequence = new SequenceCtrl(true, partialSitUps, 10);
//        partialSitUpsSet = new SetCtrl(context, partialSitUpsSequence, 3, 15000, true);
//        partialSitUpsSet.setCallback(workout);
//        partialSitUpsSequence.setCallback(partialSitUpsSet);
//        partialSitUps.setCallback(partialSitUpsSequence);
//        allSets.add(partialSitUpsSet);
//
//        allSets.add(pauseSet);
//
//        diagonalPartialSitUps = new ExerciseCtrl(context.getString(R.string.diagonal_partial_sit_ups), context.getString(R.string.both_sides), 3500, true);
//        diagonalPartialSitIpsSequence = new SequenceCtrl(true, diagonalPartialSitUps, 10);
//        diagonalPartialSitUpsSet = new SetCtrl(context, diagonalPartialSitIpsSequence, 3, 15000, true);
//        diagonalPartialSitUpsSet.setCallback(workout);
//        diagonalPartialSitIpsSequence.setCallback(diagonalPartialSitUpsSet);
//        diagonalPartialSitUps.setCallback(diagonalPartialSitIpsSequence);
//        allSets.add(diagonalPartialSitUpsSet);
//
//        allSets.add(pauseSet);
//
//        dyingBug = new ExerciseCtrl(context.getString(R.string.dying_bug), context.getString(R.string.dying_bug_foot_on_ground), 1000, true);
//        dyingBugSequence = new SequenceCtrl(true, dyingBug, 120);
//        dyingBugSet = new SetCtrl(dyingBugSequence, 1, true);
//        dyingBugSet.setCallback(workout);
//        dyingBugSequence.setCallback(dyingBugSet);
//        dyingBug.setCallback(dyingBugSequence);
//        allSets.add(dyingBugSet);
//
//        allSets.add(pauseSet);
//
//        bridge = new ExerciseCtrl(context.getString(R.string.bridge), context.getString(R.string.no_arms_hold), 10000, true);
//        bridgeSequence = new SequenceCtrl(true, bridge, 20);
//        bridgeSet = new SetCtrl(bridgeSequence, 1, true);
//        bridgeSet.setCallback(workout);
//        bridgeSequence.setCallback(bridgeSet);
//        bridge.setCallback(bridgeSequence);
//        allSets.add(bridgeSet);
//
//        allSets.add(pauseSet);
//
//        superman = new ExerciseCtrl(context.getString(R.string.superman), context.getString(R.string.alternate_opposite_arms_legs), 1750, true);
//        supermanSequence = new SequenceCtrl(true, superman, 70);
//        supermanSet = new SetCtrl(supermanSequence, 1, true);
//        supermanSet.setCallback(workout);
//        supermanSequence.setCallback(supermanSet);
//        superman.setCallback(supermanSequence);
//        allSets.add(supermanSet);
//
//        allSets.add(pauseSet);
//
//        quadruped = new ExerciseCtrl(context.getString(R.string.quadruped), 2500, true);
//        quadrupedSequence = new SequenceCtrl(true, quadruped, 50);
//        quadrupedSet = new SetCtrl(quadrupedSequence, 1, true);
//        quadrupedSet.setCallback(workout);
//        quadrupedSequence.setCallback(quadrupedSet);
//        quadruped.setCallback(quadrupedSequence);
//        allSets.add(quadrupedSet);
//
//        allSets.add(pauseSet);
//
//        wallSquat = new ExerciseCtrl(context.getString(R.string.wall_squat), 60000, true);
//        wallSquatSequence = new SequenceCtrl(true, wallSquat);
//        wallSquatSet = new SetCtrl(wallSquatSequence, 1, true);
//        wallSquatSet.setCallback(workout);
//        wallSquatSequence.setCallback(wallSquatSet);
//        wallSquat.setCallback(wallSquatSequence);
//        allSets.add(wallSquatSet);
//
//        allSets.add(pauseSet);
//
//        lunges = new ExerciseCtrl(context.getString(R.string.lunges), context.getString(R.string.partial_dip), 2500, true);
//        lungesSequence = new SequenceCtrl(true, lunges, 25);
//        lungesSet = new SetCtrl(lungesSequence, 1, true);
//        lungesSet.setCallback(workout);
//        lungesSequence.setCallback(lungesSet);
//        lunges.setCallback(lungesSequence);
//        allSets.add(lungesSet);
//
//        allSets.add(pauseSet);
//
//        pronePlank = new ExerciseCtrl(context.getString(R.string.prone_plank), context.getString(R.string.forearms), 30000, true);
//        pronePlankSequence = new SequenceCtrl(true, pronePlank);
//        pronePlankSet = new SetCtrl(pronePlankSequence, 1, true);
//        pronePlankSet.setCallback(workout);
//        pronePlankSequence.setCallback(pronePlankSet);
//        pronePlank.setCallback(pronePlankSequence);
//        allSets.add(pronePlankSet);
//
//        return allSets;
//    }
//
//    static private ArrayList<SetCtrl> getLumbarStabilizationProtocolLevel2(Context context, WorkoutCtrl workout) {
//        ArrayList<SetCtrl> allSets = new ArrayList<>();
//
//        ExerciseCtrl pauseExercise;
//        SequenceCtrl pauseSequence;
//        SetCtrl pauseSet;
//
//        ExerciseCtrl partialSitUps;
//        SequenceCtrl partialSitUpsSequence;
//        SetCtrl partialSitUpsSet;
//        ExerciseCtrl diagonalPartialSitUps;
//        SequenceCtrl diagonalPartialSitIpsSequence;
//        SetCtrl diagonalPartialSitUpsSet;
//        ExerciseCtrl dyingBug;
//        SequenceCtrl dyingBugSequence;
//        SetCtrl dyingBugSet;
//        ExerciseCtrl bridge;
//        SequenceCtrl bridgeSequence;
//        SetCtrl bridgeSet;
//        ExerciseCtrl superman;
//        SequenceCtrl supermanSequence;
//        SetCtrl supermanSet;
//        ExerciseCtrl quadruped;
//        SequenceCtrl quadrupedSequence;
//        SetCtrl quadrupedSet;
//        ExerciseCtrl wallSquat;
//        SequenceCtrl wallSquatSequence;
//        SetCtrl wallSquatSet;
//        ExerciseCtrl lunges;
//        SequenceCtrl lungesSequence;
//        SetCtrl lungesSet;
//        ExerciseCtrl pronePlank;
//        SequenceCtrl pronePlankSequence;
//        SetCtrl pronePlankSet;
//
//        pauseExercise = new ExerciseCtrl(context.getString(R.string.rest), 30000, false);
//        pauseSequence = new SequenceCtrl(false, pauseExercise);
//        pauseExercise.setRestInterval(true);
//        pauseSet = new SetCtrl(pauseSequence, 1, true);
//        pauseSet.setCallback(workout);
//        pauseExercise.setCallback(pauseSequence);
//        pauseSequence.setCallback(pauseSet);
//
//        partialSitUps = new ExerciseCtrl(context.getString(R.string.partial_sit_ups), 2500, true);
//        partialSitUps.setImageResource(R.drawable.partial_sit_ups);
//        partialSitUpsSequence = new SequenceCtrl(true, partialSitUps, 20);
//        partialSitUpsSet = new SetCtrl(context, partialSitUpsSequence, 3, 15000, true);
//        partialSitUpsSet.setCallback(workout);
//        partialSitUpsSequence.setCallback(partialSitUpsSet);
//        partialSitUps.setCallback(partialSitUpsSequence);
//        allSets.add(partialSitUpsSet);
//
//        allSets.add(pauseSet);
//
//        diagonalPartialSitUps = new ExerciseCtrl(context.getString(R.string.diagonal_partial_sit_ups), context.getString(R.string.both_sides), 3500, true);
//        diagonalPartialSitIpsSequence = new SequenceCtrl(true, diagonalPartialSitUps, 20);
//        diagonalPartialSitUpsSet = new SetCtrl(context, diagonalPartialSitIpsSequence, 3, 15000, true);
//        diagonalPartialSitUpsSet.setCallback(workout);
//        diagonalPartialSitIpsSequence.setCallback(diagonalPartialSitUpsSet);
//        diagonalPartialSitUps.setCallback(diagonalPartialSitIpsSequence);
//        allSets.add(diagonalPartialSitUpsSet);
//
//        allSets.add(pauseSet);
//
//        dyingBug = new ExerciseCtrl(context.getString(R.string.dying_bug), context.getString(R.string.heel_tap), 1000, true);
//        dyingBugSequence = new SequenceCtrl(true, dyingBug, 120);
//        dyingBugSet = new SetCtrl(dyingBugSequence, 1, true);
//        dyingBugSet.setCallback(workout);
//        dyingBugSequence.setCallback(dyingBugSet);
//        dyingBug.setCallback(dyingBugSequence);
//        allSets.add(dyingBugSet);
//
//        allSets.add(pauseSet);
//
//        bridge = new ExerciseCtrl(context.getString(R.string.bridge), context.getString(R.string.no_arms_alternate_legs_hold), 10000, true);
//        bridgeSequence = new SequenceCtrl(true, bridge, 20);
//        bridgeSet = new SetCtrl(bridgeSequence, 1, true);
//        bridgeSet.setCallback(workout);
//        bridgeSequence.setCallback(bridgeSet);
//        bridge.setCallback(bridgeSequence);
//        allSets.add(bridgeSet);
//
//        allSets.add(pauseSet);
//
//        superman = new ExerciseCtrl(context.getString(R.string.superman), context.getString(R.string.elbows_bent), 1750, true);
//        supermanSequence = new SequenceCtrl(true, superman, 70);
//        supermanSet = new SetCtrl(supermanSequence, 1, true);
//        supermanSet.setCallback(workout);
//        supermanSequence.setCallback(supermanSet);
//        superman.setCallback(supermanSequence);
//        allSets.add(supermanSet);
//
//        allSets.add(pauseSet);
//
//        quadruped = new ExerciseCtrl(context.getString(R.string.quadruped), context.getString(R.string.hold), 15000, true);
//        quadrupedSequence = new SequenceCtrl(true, quadruped, 10);
//        quadrupedSet = new SetCtrl(quadrupedSequence, 1, true);
//        quadrupedSet.setCallback(workout);
//        quadrupedSequence.setCallback(quadrupedSet);
//        quadruped.setCallback(quadrupedSequence);
//        allSets.add(quadrupedSet);
//
//        allSets.add(pauseSet);
//
//        wallSquat = new ExerciseCtrl(context.getString(R.string.wall_squat), 90000, true);
//        wallSquatSequence = new SequenceCtrl(true, wallSquat);
//        wallSquatSet = new SetCtrl(wallSquatSequence, 1, true);
//        wallSquatSet.setCallback(workout);
//        wallSquatSequence.setCallback(wallSquatSet);
//        wallSquat.setCallback(wallSquatSequence);
//        allSets.add(wallSquatSet);
//
//        allSets.add(pauseSet);
//
//        lunges = new ExerciseCtrl(context.getString(R.string.lunges), context.getString(R.string.partial_dip_hold), 15000, true);
//        lungesSequence = new SequenceCtrl(true, lunges, 10);
//        lungesSet = new SetCtrl(lungesSequence, 1, true);
//        lungesSet.setCallback(workout);
//        lungesSequence.setCallback(lungesSet);
//        lunges.setCallback(lungesSequence);
//        allSets.add(lungesSet);
//
//        allSets.add(pauseSet);
//
//        pronePlank = new ExerciseCtrl(context.getString(R.string.prone_plank), context.getString(R.string.forearms), 60000, true);
//        pronePlankSequence = new SequenceCtrl(true, pronePlank);
//        pronePlankSet = new SetCtrl(pronePlankSequence, 1, true);
//        pronePlankSet.setCallback(workout);
//        pronePlankSequence.setCallback(pronePlankSet);
//        pronePlank.setCallback(pronePlankSequence);
//        allSets.add(pronePlankSet);
//
//        return allSets;
//    }
//
//    static private ArrayList<SetCtrl> getLumbarStabilizationProtocolLevel3(Context context, WorkoutCtrl workout) {
//        ArrayList<SetCtrl> allSets = new ArrayList<>();
//
//        ExerciseCtrl pauseExercise;
//        SequenceCtrl pauseSequence;
//        SetCtrl pauseSet;
//
//        ExerciseCtrl partialSitUps;
//        SequenceCtrl partialSitUpsSequence;
//        SetCtrl partialSitUpsSet;
//        ExerciseCtrl diagonalPartialSitUps;
//        SequenceCtrl diagonalPartialSitIpsSequence;
//        SetCtrl diagonalPartialSitUpsSet;
//        ExerciseCtrl dyingBug;
//        SequenceCtrl dyingBugSequence;
//        SetCtrl dyingBugSet;
//        ExerciseCtrl bridge;
//        SequenceCtrl bridgeSequence;
//        SetCtrl bridgeSet;
//        ExerciseCtrl superman;
//        SequenceCtrl supermanSequence;
//        SetCtrl supermanSet;
//        ExerciseCtrl quadruped;
//        SequenceCtrl quadrupedSequence;
//        SetCtrl quadrupedSet;
//        ExerciseCtrl wallSquat;
//        SequenceCtrl wallSquatSequence;
//        SetCtrl wallSquatSet;
//        ExerciseCtrl lunges;
//        SequenceCtrl lungesSequence;
//        SetCtrl lungesSet;
//        ExerciseCtrl pronePlank;
//        SequenceCtrl pronePlankSequence;
//        SetCtrl pronePlankSet;
//
//        pauseExercise = new ExerciseCtrl(context.getString(R.string.rest), 30000, false);
//        pauseSequence = new SequenceCtrl(false, pauseExercise);
//        pauseExercise.setRestInterval(true);
//        pauseSet = new SetCtrl(pauseSequence, 1, true);
//        pauseSet.setCallback(workout);
//        pauseExercise.setCallback(pauseSequence);
//        pauseSequence.setCallback(pauseSet);
//
//        partialSitUps = new ExerciseCtrl(context.getString(R.string.partial_sit_ups), context.getString(R.string._1kg_chest), 2500, true);
//        partialSitUps.setImageResource(R.drawable.partial_sit_ups);
//        partialSitUpsSequence = new SequenceCtrl(true, partialSitUps, 30);
//        partialSitUpsSet = new SetCtrl(context, partialSitUpsSequence, 3, 15000, true);
//        partialSitUpsSet.setCallback(workout);
//        partialSitUpsSequence.setCallback(partialSitUpsSet);
//        partialSitUps.setCallback(partialSitUpsSequence);
//        allSets.add(partialSitUpsSet);
//
//        allSets.add(pauseSet);
//
//        diagonalPartialSitUps = new ExerciseCtrl(context.getString(R.string.diagonal_partial_sit_ups), context.getString(R.string.both_sides_1kg_chest), 3500, true);
//        diagonalPartialSitIpsSequence = new SequenceCtrl(true, diagonalPartialSitUps, 30);
//        diagonalPartialSitUpsSet = new SetCtrl(context, diagonalPartialSitIpsSequence, 3, 15000, true);
//        diagonalPartialSitUpsSet.setCallback(workout);
//        diagonalPartialSitIpsSequence.setCallback(diagonalPartialSitUpsSet);
//        diagonalPartialSitUps.setCallback(diagonalPartialSitIpsSequence);
//        allSets.add(diagonalPartialSitUpsSet);
//
//        allSets.add(pauseSet);
//
//        dyingBug = new ExerciseCtrl(context.getString(R.string.dying_bug), context.getString(R.string.heel_tap), 1000, true);
//        dyingBugSequence = new SequenceCtrl(true, dyingBug, 180);
//        dyingBugSet = new SetCtrl(dyingBugSequence, 1, true);
//        dyingBugSet.setCallback(workout);
//        dyingBugSequence.setCallback(dyingBugSet);
//        dyingBug.setCallback(dyingBugSequence);
//        allSets.add(dyingBugSet);
//
//        allSets.add(pauseSet);
//
//        bridge = new ExerciseCtrl(context.getString(R.string.bridge), context.getString(R.string.no_arms_alternate_legs_hold), 10000, true);
//        bridgeSequence = new SequenceCtrl(true, bridge, 25);
//        bridgeSet = new SetCtrl(bridgeSequence, 1, true);
//        bridgeSet.setCallback(workout);
//        bridgeSequence.setCallback(bridgeSet);
//        bridge.setCallback(bridgeSequence);
//        allSets.add(bridgeSet);
//
//        allSets.add(pauseSet);
//
//        superman = new ExerciseCtrl(context.getString(R.string.superman), context.getString(R.string.elbows_bent_1kg_legs), 1750, true);
//        supermanSequence = new SequenceCtrl(true, superman, 100);
//        supermanSet = new SetCtrl(supermanSequence, 1, true);
//        supermanSet.setCallback(workout);
//        supermanSequence.setCallback(supermanSet);
//        superman.setCallback(supermanSequence);
//        allSets.add(supermanSet);
//
//        allSets.add(pauseSet);
//
//        quadruped = new ExerciseCtrl(context.getString(R.string.quadruped), context.getString(R.string.hold_2kg_legs), 15000, true);
//        quadrupedSequence = new SequenceCtrl(true, quadruped, 15);
//        quadrupedSet = new SetCtrl(quadrupedSequence, 1, true);
//        quadrupedSet.setCallback(workout);
//        quadrupedSequence.setCallback(quadrupedSet);
//        quadruped.setCallback(quadrupedSequence);
//        allSets.add(quadrupedSet);
//
//        allSets.add(pauseSet);
//
//        wallSquat = new ExerciseCtrl(context.getString(R.string.wall_squat), 120000, true);
//        wallSquatSequence = new SequenceCtrl(true, wallSquat);
//        wallSquatSet = new SetCtrl(wallSquatSequence, 1, true);
//        wallSquatSet.setCallback(workout);
//        wallSquatSequence.setCallback(wallSquatSet);
//        wallSquat.setCallback(wallSquatSequence);
//        allSets.add(wallSquatSet);
//
//        allSets.add(pauseSet);
//
//        lunges = new ExerciseCtrl(context.getString(R.string.lunges), context.getString(R.string.fast_full_dip_hold), 15000, true);
//        lungesSequence = new SequenceCtrl(true, lunges, 15);
//        lungesSet = new SetCtrl(lungesSequence, 1, true);
//        lungesSet.setCallback(workout);
//        lungesSequence.setCallback(lungesSet);
//        lunges.setCallback(lungesSequence);
//        allSets.add(lungesSet);
//
//        allSets.add(pauseSet);
//
//        pronePlank = new ExerciseCtrl(context.getString(R.string.prone_plank), context.getString(R.string.forearms), 120000, true);
//        pronePlankSequence = new SequenceCtrl(true, pronePlank);
//        pronePlankSet = new SetCtrl(pronePlankSequence, 1, true);
//        pronePlankSet.setCallback(workout);
//        pronePlankSequence.setCallback(pronePlankSet);
//        pronePlank.setCallback(pronePlankSequence);
//        allSets.add(pronePlankSet);
//
//        return allSets;
//    }
//
//    static private ArrayList<SetCtrl> getLumbarStabilizationProtocolLevel4(Context context, WorkoutCtrl workout) {
//        ArrayList<SetCtrl> allSets = new ArrayList<>();
//
//        ExerciseCtrl pauseExercise;
//        SequenceCtrl pauseSequence;
//        SetCtrl pauseSet;
//
//        ExerciseCtrl partialSitUps;
//        SequenceCtrl partialSitUpsSequence;
//        SetCtrl partialSitUpsSet;
//        ExerciseCtrl diagonalPartialSitUps;
//        SequenceCtrl diagonalPartialSitIpsSequence;
//        SetCtrl diagonalPartialSitUpsSet;
//        ExerciseCtrl dyingBug;
//        SequenceCtrl dyingBugSequence;
//        SetCtrl dyingBugSet;
//        ExerciseCtrl bridge;
//        SequenceCtrl bridgeSequence;
//        SetCtrl bridgeSet;
//        ExerciseCtrl superman;
//        SequenceCtrl supermanSequence;
//        SetCtrl supermanSet;
//        ExerciseCtrl quadruped;
//        SequenceCtrl quadrupedSequence;
//        SetCtrl quadrupedSet;
//        ExerciseCtrl wallSquat;
//        SequenceCtrl wallSquatSequence;
//        SetCtrl wallSquatSet;
//        ExerciseCtrl lunges;
//        SequenceCtrl lungesSequence;
//        SetCtrl lungesSet;
//        ExerciseCtrl pronePlank;
//        SequenceCtrl pronePlankSequence;
//        SetCtrl pronePlankSet;
//
//        pauseExercise = new ExerciseCtrl(context.getString(R.string.rest), 30000, false);
//        pauseSequence = new SequenceCtrl(false, pauseExercise);
//        pauseExercise.setRestInterval(true);
//        pauseSet = new SetCtrl(pauseSequence, 1, true);
//        pauseSet.setCallback(workout);
//        pauseExercise.setCallback(pauseSequence);
//        pauseSequence.setCallback(pauseSet);
//
//        partialSitUps = new ExerciseCtrl(context.getString(R.string.partial_sit_ups), context.getString(R.string._2kg_chest), 2500, true);
//        partialSitUps.setImageResource(R.drawable.partial_sit_ups);
//        partialSitUpsSequence = new SequenceCtrl(true, partialSitUps, 30);
//        partialSitUpsSet = new SetCtrl(context, partialSitUpsSequence, 3, 15000, true);
//        partialSitUpsSet.setCallback(workout);
//        partialSitUpsSequence.setCallback(partialSitUpsSet);
//        partialSitUps.setCallback(partialSitUpsSequence);
//        allSets.add(partialSitUpsSet);
//
//        allSets.add(pauseSet);
//
//        diagonalPartialSitUps = new ExerciseCtrl(context.getString(R.string.diagonal_partial_sit_ups), context.getString(R.string.both_sides_2kg_chest), 3500, true);
//        diagonalPartialSitIpsSequence = new SequenceCtrl(true, diagonalPartialSitUps, 30);
//        diagonalPartialSitUpsSet = new SetCtrl(context, diagonalPartialSitIpsSequence, 3, 15000, true);
//        diagonalPartialSitUpsSet.setCallback(workout);
//        diagonalPartialSitIpsSequence.setCallback(diagonalPartialSitUpsSet);
//        diagonalPartialSitUps.setCallback(diagonalPartialSitIpsSequence);
//        allSets.add(diagonalPartialSitUpsSet);
//
//        allSets.add(pauseSet);
//
//        dyingBug = new ExerciseCtrl(context.getString(R.string.dying_bug), context.getString(R.string._1_5kg), 1000, true);
//        dyingBugSequence = new SequenceCtrl(true, dyingBug, 300);
//        dyingBugSet = new SetCtrl(dyingBugSequence, 1, true);
//        dyingBugSet.setCallback(workout);
//        dyingBugSequence.setCallback(dyingBugSet);
//        dyingBug.setCallback(dyingBugSequence);
//        allSets.add(dyingBugSet);
//
//        allSets.add(pauseSet);
//
//        bridge = new ExerciseCtrl(context.getString(R.string.bridge), context.getString(R.string.no_arms_alternate_legs_hold), 10000, true);
//        bridgeSequence = new SequenceCtrl(true, bridge, 30);
//        bridgeSet = new SetCtrl(bridgeSequence, 1, true);
//        bridgeSet.setCallback(workout);
//        bridgeSequence.setCallback(bridgeSet);
//        bridge.setCallback(bridgeSequence);
//        allSets.add(bridgeSet);
//
//        allSets.add(pauseSet);
//
//        superman = new ExerciseCtrl(context.getString(R.string.superman), context.getString(R.string.elbows_bent_1_5kg_legs), 1750, true);
//        supermanSequence = new SequenceCtrl(true, superman, 140);
//        supermanSet = new SetCtrl(supermanSequence, 1, true);
//        supermanSet.setCallback(workout);
//        supermanSequence.setCallback(supermanSet);
//        superman.setCallback(supermanSequence);
//        allSets.add(supermanSet);
//
//        allSets.add(pauseSet);
//
//        quadruped = new ExerciseCtrl(context.getString(R.string.quadruped), context.getString(R.string.hold_2kg_legs_1_5kg_arms), 15000, true);
//        quadrupedSequence = new SequenceCtrl(true, quadruped, 20);
//        quadrupedSet = new SetCtrl(quadrupedSequence, 1, true);
//        quadrupedSet.setCallback(workout);
//        quadrupedSequence.setCallback(quadrupedSet);
//        quadruped.setCallback(quadrupedSequence);
//        allSets.add(quadrupedSet);
//
//        allSets.add(pauseSet);
//
//        wallSquat = new ExerciseCtrl(context.getString(R.string.wall_squat), 180000, true);
//        wallSquatSequence = new SequenceCtrl(true, wallSquat);
//        wallSquatSet = new SetCtrl(wallSquatSequence, 1, true);
//        wallSquatSet.setCallback(workout);
//        wallSquatSequence.setCallback(wallSquatSet);
//        wallSquat.setCallback(wallSquatSequence);
//        allSets.add(wallSquatSet);
//
//        allSets.add(pauseSet);
//
//        lunges = new ExerciseCtrl(context.getString(R.string.lunges), context.getString(R.string.fast_full_dip_hold_1_5kg_arms), 15000, true);
//        lungesSequence = new SequenceCtrl(true, lunges, 15);
//        lungesSet = new SetCtrl(lungesSequence, 1, true);
//        lungesSet.setCallback(workout);
//        lungesSequence.setCallback(lungesSet);
//        lunges.setCallback(lungesSequence);
//        allSets.add(lungesSet);
//
//        allSets.add(pauseSet);
//
//        pronePlank = new ExerciseCtrl(context.getString(R.string.prone_plank), context.getString(R.string.forearms), 180000, true);
//        pronePlankSequence = new SequenceCtrl(true, pronePlank);
//        pronePlankSet = new SetCtrl(pronePlankSequence, 1, true);
//        pronePlankSet.setCallback(workout);
//        pronePlankSequence.setCallback(pronePlankSet);
//        pronePlank.setCallback(pronePlankSequence);
//        allSets.add(pronePlankSet);
//
//        return allSets;
//    }
//
//    static private ArrayList<SetCtrl> getLumbarStabilizationProtocolLevel5(Context context, WorkoutCtrl workout) {
//        ArrayList<SetCtrl> allSets = new ArrayList<>();
//
//        ExerciseCtrl pauseExercise;
//        SequenceCtrl pauseSequence;
//        SetCtrl pauseSet;
//
//        ExerciseCtrl partialSitUps;
//        SequenceCtrl partialSitUpsSequence;
//        SetCtrl partialSitUpsSet;
//        ExerciseCtrl diagonalPartialSitUps;
//        SequenceCtrl diagonalPartialSitIpsSequence;
//        SetCtrl diagonalPartialSitUpsSet;
//        ExerciseCtrl dyingBug;
//        SequenceCtrl dyingBugSequence;
//        SetCtrl dyingBugSet;
//        ExerciseCtrl bridge;
//        SequenceCtrl bridgeSequence;
//        SetCtrl bridgeSet;
//        ExerciseCtrl superman;
//        SequenceCtrl supermanSequence;
//        SetCtrl supermanSet;
//        ExerciseCtrl quadruped;
//        SequenceCtrl quadrupedSequence;
//        SetCtrl quadrupedSet;
//        ExerciseCtrl wallSquat;
//        SequenceCtrl wallSquatSequence;
//        SetCtrl wallSquatSet;
//        ExerciseCtrl lunges;
//        SequenceCtrl lungesSequence;
//        SetCtrl lungesSet;
//        ExerciseCtrl pronePlank;
//        SequenceCtrl pronePlankSequence;
//        SetCtrl pronePlankSet;
//
//        pauseExercise = new ExerciseCtrl(context.getString(R.string.rest), 30000, false);
//        pauseSequence = new SequenceCtrl(false, pauseExercise);
//        pauseExercise.setRestInterval(true);
//        pauseSet = new SetCtrl(pauseSequence, 1, true);
//        pauseSet.setCallback(workout);
//        pauseExercise.setCallback(pauseSequence);
//        pauseSequence.setCallback(pauseSet);
//
//        partialSitUps = new ExerciseCtrl(context.getString(R.string.partial_sit_ups), context.getString(R.string._2kg_chest), 2500, true);
//        partialSitUps.setImageResource(R.drawable.partial_sit_ups);
//        partialSitUpsSequence = new SequenceCtrl(true, partialSitUps, 50);
//        partialSitUpsSet = new SetCtrl(context, partialSitUpsSequence, 3, 15000, true);
//        partialSitUpsSet.setCallback(workout);
//        partialSitUpsSequence.setCallback(partialSitUpsSet);
//        partialSitUps.setCallback(partialSitUpsSequence);
//        allSets.add(partialSitUpsSet);
//
//        allSets.add(pauseSet);
//
//        diagonalPartialSitUps = new ExerciseCtrl(context.getString(R.string.diagonal_partial_sit_ups), context.getString(R.string.both_sides_2kg_chest), 3500, true);
//        diagonalPartialSitIpsSequence = new SequenceCtrl(true, diagonalPartialSitUps, 50);
//        diagonalPartialSitUpsSet = new SetCtrl(context, diagonalPartialSitIpsSequence, 3, 15000, true);
//        diagonalPartialSitUpsSet.setCallback(workout);
//        diagonalPartialSitIpsSequence.setCallback(diagonalPartialSitUpsSet);
//        diagonalPartialSitUps.setCallback(diagonalPartialSitIpsSequence);
//        allSets.add(diagonalPartialSitUpsSet);
//
//        allSets.add(pauseSet);
//
//        dyingBug = new ExerciseCtrl(context.getString(R.string.dying_bug), context.getString(R.string._2kg), 1000, true);
//        dyingBugSequence = new SequenceCtrl(true, dyingBug, 600);
//        dyingBugSet = new SetCtrl(dyingBugSequence, 1, true);
//        dyingBugSet.setCallback(workout);
//        dyingBugSequence.setCallback(dyingBugSet);
//        dyingBug.setCallback(dyingBugSequence);
//        allSets.add(dyingBugSet);
//
//        allSets.add(pauseSet);
//
//        bridge = new ExerciseCtrl(context.getString(R.string.bridge), context.getString(R.string.no_arms_alternate_legs_hold), 10000, true);
//        bridgeSequence = new SequenceCtrl(true, bridge, 45);
//        bridgeSet = new SetCtrl(bridgeSequence, 1, true);
//        bridgeSet.setCallback(workout);
//        bridgeSequence.setCallback(bridgeSet);
//        bridge.setCallback(bridgeSequence);
//        allSets.add(bridgeSet);
//
//        allSets.add(pauseSet);
//
//        superman = new ExerciseCtrl(context.getString(R.string.superman), context.getString(R.string.elbows_bent_2kg_legs), 1750, true);
//        supermanSequence = new SequenceCtrl(true, superman, 140);
//        supermanSet = new SetCtrl(supermanSequence, 1, true);
//        supermanSet.setCallback(workout);
//        supermanSequence.setCallback(supermanSet);
//        superman.setCallback(supermanSequence);
//        allSets.add(supermanSet);
//
//        allSets.add(pauseSet);
//
//        quadruped = new ExerciseCtrl(context.getString(R.string.quadruped), context.getString(R.string.hold_2kg_legs_1_5kg_arms), 15000, true);
//        quadrupedSequence = new SequenceCtrl(true, quadruped, 25);
//        quadrupedSet = new SetCtrl(quadrupedSequence, 1, true);
//        quadrupedSet.setCallback(workout);
//        quadrupedSequence.setCallback(quadrupedSet);
//        quadruped.setCallback(quadrupedSequence);
//        allSets.add(quadrupedSet);
//
//        allSets.add(pauseSet);
//
//        wallSquat = new ExerciseCtrl(context.getString(R.string.wall_squat), 300000, true);
//        wallSquatSequence = new SequenceCtrl(true, wallSquat);
//        wallSquatSet = new SetCtrl(wallSquatSequence, 1, true);
//        wallSquatSet.setCallback(workout);
//        wallSquatSequence.setCallback(wallSquatSet);
//        wallSquat.setCallback(wallSquatSequence);
//        allSets.add(wallSquatSet);
//
//        allSets.add(pauseSet);
//
//        lunges = new ExerciseCtrl(context.getString(R.string.lunges), context.getString(R.string.fast_full_dip_hold_2kg_arms), 15000, true);
//        lungesSequence = new SequenceCtrl(true, lunges, 25);
//        lungesSet = new SetCtrl(lungesSequence, 1, true);
//        lungesSet.setCallback(workout);
//        lungesSequence.setCallback(lungesSet);
//        lunges.setCallback(lungesSequence);
//        allSets.add(lungesSet);
//
//        allSets.add(pauseSet);
//
//        pronePlank = new ExerciseCtrl(context.getString(R.string.prone_plank), context.getString(R.string.forearms), 300000, true);
//        pronePlankSequence = new SequenceCtrl(true, pronePlank);
//        pronePlankSet = new SetCtrl(pronePlankSequence, 1, true);
//        pronePlankSet.setCallback(workout);
//        pronePlankSequence.setCallback(pronePlankSet);
//        pronePlank.setCallback(pronePlankSequence);
//        allSets.add(pronePlankSet);
//
//        return allSets;
//    }
//
//
//    static private ArrayList<SetCtrl> getHighIntensityIntervalTraining8min(Context context, WorkoutCtrl workout) {
//        ArrayList<SetCtrl> allSets = new ArrayList<>();
//
//        ExerciseCtrl pauseExercise;
//        SequenceCtrl pauseSequence;
//        SetCtrl pauseSet;
//
//        ExerciseCtrl jumpingJack;
//        SequenceCtrl jumpingJackSequence;
//        SetCtrl jumpingJackSet;
//        ExerciseCtrl wallSit;
//        SequenceCtrl wallSitSequence;
//        SetCtrl wallSitSet;
//        ExerciseCtrl pushUp;
//        SequenceCtrl pushUpSequence;
//        SetCtrl pushUpSet;
//        ExerciseCtrl partialSitUp;
//        SequenceCtrl partialSitUpSequence;
//        SetCtrl partialSitUpSet;
//        ExerciseCtrl stepUp;
//        SequenceCtrl stepUpSequence;
//        SetCtrl stepUpSet;
//        ExerciseCtrl squat;
//        SequenceCtrl squatSequence;
//        SetCtrl squatSet;
//        ExerciseCtrl tricepsDip;
//        SequenceCtrl tricepsDipSequence;
//        SetCtrl tricepsDipSet;
//        ExerciseCtrl pronePlank;
//        SequenceCtrl pronePlankSequence;
//        SetCtrl pronePlankSet;
//        ExerciseCtrl highKneesRun;
//        SequenceCtrl highKneesRunSequence;
//        SetCtrl highKneesRunSet;
//        ExerciseCtrl lunge;
//        SequenceCtrl lungeSequence;
//        SetCtrl lungeSet;
//        ExerciseCtrl pushUp2;
//        SequenceCtrl pushUp2Sequence;
//        SetCtrl pushUp2Set;
//        ExerciseCtrl sidePlank;
//        SequenceCtrl sidePlankSequence;
//        SetCtrl sidePlankSet;
//
//        pauseExercise = new ExerciseCtrl(context.getString(R.string.rest), 10000, false);
//        pauseSequence = new SequenceCtrl(false, pauseExercise);
//        pauseExercise.setRestInterval(true);
//        pauseSet = new SetCtrl(pauseSequence, 1, true);
//        pauseSet.setCallback(workout);
//        pauseExercise.setCallback(pauseSequence);
//        pauseSequence.setCallback(pauseSet);
//
//        jumpingJack = new ExerciseCtrl(context.getString(R.string.jumping_jacks), 750, true);
//        jumpingJackSequence = new SequenceCtrl(true, jumpingJack, 40);
//        jumpingJackSet = new SetCtrl(jumpingJackSequence, 1, true);
//        jumpingJackSet.setCallback(workout);
//        jumpingJackSequence.setCallback(jumpingJackSet);
//        jumpingJack.setCallback(jumpingJackSequence);
//        allSets.add(jumpingJackSet);
//
//        allSets.add(pauseSet);
//
//        wallSit = new ExerciseCtrl(context.getString(R.string.wall_squat), 30000, true);
//        wallSitSequence = new SequenceCtrl(true, wallSit);
//        wallSitSet = new SetCtrl(wallSitSequence, 1, true);
//        wallSitSet.setCallback(workout);
//        wallSitSequence.setCallback(wallSitSet);
//        wallSit.setCallback(wallSitSequence);
//        allSets.add(wallSitSet);
//
//        allSets.add(pauseSet);
//
//        pushUp = new ExerciseCtrl(context.getString(R.string.push_ups), 1000, true);
//        pushUpSequence = new SequenceCtrl(true, pushUp, 30);
//        pushUpSet = new SetCtrl(pushUpSequence, 1, true);
//        pushUpSet.setCallback(workout);
//        pushUpSequence.setCallback(pushUpSet);
//        pushUp.setCallback(pushUpSequence);
//        allSets.add(pushUpSet);
//
//        allSets.add(pauseSet);
//
//        partialSitUp = new ExerciseCtrl(context.getString(R.string.partial_sit_ups), 1500, true);
//        partialSitUp.setImageResource(R.drawable.partial_sit_ups);
//        partialSitUpSequence = new SequenceCtrl(true, partialSitUp, 20);
//        partialSitUpSet = new SetCtrl(partialSitUpSequence, 1, true);
//        partialSitUpSet.setCallback(workout);
//        partialSitUpSequence.setCallback(partialSitUpSet);
//        partialSitUp.setCallback(partialSitUpSequence);
//        allSets.add(partialSitUpSet);
//
//        allSets.add(pauseSet);
//
//        stepUp = new ExerciseCtrl(context.getString(R.string.step_ups), 1500, true);
//        stepUpSequence = new SequenceCtrl(true, stepUp, 20);
//        stepUpSet = new SetCtrl(stepUpSequence, 1, true);
//        stepUpSet.setCallback(workout);
//        stepUpSequence.setCallback(stepUpSet);
//        stepUp.setCallback(stepUpSequence);
//        allSets.add(stepUpSet);
//
//        allSets.add(pauseSet);
//
//        squat = new ExerciseCtrl(context.getString(R.string.squats), 1200, true);
//        squatSequence = new SequenceCtrl(true, squat, 25);
//        squatSet = new SetCtrl(squatSequence, 1, true);
//        squatSet.setCallback(workout);
//        squatSequence.setCallback(squatSet);
//        squat.setCallback(squatSequence);
//        allSets.add(squatSet);
//
//        allSets.add(pauseSet);
//
//        tricepsDip = new ExerciseCtrl(context.getString(R.string.triceps_dips), 1200, true);
//        tricepsDipSequence = new SequenceCtrl(true, tricepsDip, 25);
//        tricepsDipSet = new SetCtrl(tricepsDipSequence, 1, true);
//        tricepsDipSet.setCallback(workout);
//        tricepsDipSequence.setCallback(tricepsDipSet);
//        tricepsDip.setCallback(tricepsDipSequence);
//        allSets.add(tricepsDipSet);
//
//        allSets.add(pauseSet);
//
//        pronePlank = new ExerciseCtrl(context.getString(R.string.prone_plank), context.getString(R.string.forearms), 30000, true);
//        pronePlankSequence = new SequenceCtrl(true, pronePlank);
//        pronePlankSet = new SetCtrl(pronePlankSequence, 1, true);
//        pronePlankSet.setCallback(workout);
//        pronePlankSequence.setCallback(pronePlankSet);
//        pronePlank.setCallback(pronePlankSequence);
//        allSets.add(pronePlankSet);
//
//        allSets.add(pauseSet);
//
//        highKneesRun = new ExerciseCtrl(context.getString(R.string.high_knees_run), context.getString(R.string.on_place), 600, true);
//        highKneesRunSequence = new SequenceCtrl(true, highKneesRun, 50);
//        highKneesRunSet = new SetCtrl(highKneesRunSequence, 1, true);
//        highKneesRunSet.setCallback(workout);
//        highKneesRunSequence.setCallback(highKneesRunSet);
//        highKneesRun.setCallback(highKneesRunSequence);
//        allSets.add(highKneesRunSet);
//
//        allSets.add(pauseSet);
//
//        lunge = new ExerciseCtrl(context.getString(R.string.lunges), context.getString(R.string.full_dip), 1500, true);
//        lungeSequence = new SequenceCtrl(true, lunge, 20);
//        lungeSet = new SetCtrl(lungeSequence, 1, true);
//        lungeSet.setCallback(workout);
//        lungeSequence.setCallback(lungeSet);
//        lunge.setCallback(lungeSequence);
//        allSets.add(lungeSet);
//
//        allSets.add(pauseSet);
//
//        pushUp2 = new ExerciseCtrl(context.getString(R.string.push_ups), context.getString(R.string._and_rotation), 2000, true);
//        pushUp2Sequence = new SequenceCtrl(true, pushUp2, 23);
//        pushUp2Set = new SetCtrl(pushUp2Sequence, 1, true);
//        pushUp2Set.setCallback(workout);
//        pushUp2Sequence.setCallback(pushUp2Set);
//        pushUp2.setCallback(pushUp2Sequence);
//        allSets.add(pushUp2Set);
//
//        allSets.add(pauseSet);
//
//        sidePlank = new ExerciseCtrl(context.getString(R.string.side_plank), context.getString(R.string.left_side), 30000, true);
//        sidePlankSequence = new SequenceCtrl(true, sidePlank);
//        sidePlankSet = new SetCtrl(sidePlankSequence, 1, true);
//        sidePlankSet.setCallback(workout);
//        sidePlankSequence.setCallback(sidePlankSet);
//        sidePlank.setCallback(sidePlankSequence);
//        allSets.add(sidePlankSet);
//
//        allSets.add(pauseSet);
//
//        sidePlank = new ExerciseCtrl(context.getString(R.string.side_plank), context.getString(R.string.right_side), 30000, true);
//        sidePlankSequence = new SequenceCtrl(true, sidePlank);
//        sidePlankSet = new SetCtrl(sidePlankSequence, 1, true);
//        sidePlankSet.setCallback(workout);
//        sidePlankSequence.setCallback(sidePlankSet);
//        sidePlank.setCallback(sidePlankSequence);
//        allSets.add(sidePlankSet);
//
//        return allSets;
//    }
//
//    static private ArrayList<SetCtrl> getHighIntensityIntervalTraining10min(Context context, WorkoutCtrl workout) {
//        ArrayList<SetCtrl> allSets = new ArrayList<>();
//
//        ExerciseCtrl pauseExercise;
//        SequenceCtrl pauseSequence;
//        SetCtrl pauseSet;
//
//        ExerciseCtrl jabCrossFrontR;
//        SequenceCtrl jabCrossFrontRSequence;
//        SetCtrl jabCrossFrontRSet;
//        ExerciseCtrl jabCrossFrontL;
//        SequenceCtrl jabCrossFrontLSequence;
//        SetCtrl jabCrossFrontLSet;
//        ExerciseCtrl jumpingJack;
//        SequenceCtrl jumpingJackSequence;
//        SetCtrl jumpingJackSet;
//        ExerciseCtrl squat;
//        SequenceCtrl squatSequence;
//        SetCtrl squatSet;
//
//        pauseExercise = new ExerciseCtrl(context.getString(R.string.rest), 30000, false);
//        pauseSequence = new SequenceCtrl(false, pauseExercise);
//        pauseExercise.setRestInterval(true);
//        pauseSet = new SetCtrl(pauseSequence, 1, true);
//        pauseSet.setCallback(workout);
//        pauseExercise.setCallback(pauseSequence);
//        pauseSequence.setCallback(pauseSet);
//
//        jabCrossFrontR = new ExerciseCtrl(context.getString(R.string.jab_cross_front_kick), context.getString(R.string.right_side), 1360, true);
//        jabCrossFrontRSequence = new SequenceCtrl(true, jabCrossFrontR, 15);
//        jabCrossFrontRSet = new SetCtrl(context, jabCrossFrontRSequence, 3, 10000, true);
//        jabCrossFrontRSet.setCallback(workout);
//        jabCrossFrontRSequence.setCallback(jabCrossFrontRSet);
//        jabCrossFrontR.setCallback(jabCrossFrontRSequence);
//        allSets.add(jabCrossFrontRSet);
//
//        allSets.add(pauseSet);
//
//        jabCrossFrontL = new ExerciseCtrl(context.getString(R.string.jab_cross_front_kick), context.getString(R.string.left_side), 1360, true);
//        jabCrossFrontLSequence = new SequenceCtrl(true, jabCrossFrontL, 15);
//        jabCrossFrontLSet = new SetCtrl(context, jabCrossFrontLSequence, 3, 10000, true);
//        jabCrossFrontLSet.setCallback(workout);
//        jabCrossFrontLSequence.setCallback(jabCrossFrontLSet);
//        jabCrossFrontL.setCallback(jabCrossFrontLSequence);
//        allSets.add(jabCrossFrontLSet);
//
//        allSets.add(pauseSet);
//
//        jumpingJack = new ExerciseCtrl(context.getString(R.string.jumping_jacks), 750, true);
//        jumpingJackSequence = new SequenceCtrl(true, jumpingJack, 27);
//        jumpingJackSet = new SetCtrl(context, jumpingJackSequence, 3, 10000, true);
//        jumpingJackSet.setCallback(workout);
//        jumpingJackSequence.setCallback(jumpingJackSet);
//        jumpingJack.setCallback(jumpingJackSequence);
//        allSets.add(jumpingJackSet);
//
//        allSets.add(pauseSet);
//
//        squat = new ExerciseCtrl(context.getString(R.string.squats), context.getString(R.string.sumo_style), 1200, true);
//        squatSequence = new SequenceCtrl(true, squat, 17);
//        squatSet = new SetCtrl(context, squatSequence, 3, 10000, true);
//        squatSet.setCallback(workout);
//        squatSequence.setCallback(squatSet);
//        squat.setCallback(squatSequence);
//        allSets.add(squatSet);
//
//        return allSets;
//    }
//
//    static private ArrayList<SetCtrl> getHighIntensityIntervalTraining20min(Context context, WorkoutCtrl workout) {
//        ArrayList<SetCtrl> allSets = new ArrayList<>();
//
//        ExerciseCtrl pauseExercise;
//        SequenceCtrl pauseSequence;
//        SetCtrl pauseSet;
//
//        ExerciseCtrl pushUp;
//        SequenceCtrl pushUpSequence;
//        SetCtrl pushUpSet;
//        ExerciseCtrl squat;
//        SequenceCtrl squatSequence;
//        SetCtrl squatSet;
//        ExerciseCtrl buttKick;
//        SequenceCtrl buttKickSequence;
//        SetCtrl buttKickSet;
//        ExerciseCtrl tricepsDip;
//        SequenceCtrl tricepsDipSequence;
//        SetCtrl tricepsDipSet;
//        ExerciseCtrl sideLunge;
//        SequenceCtrl sideLungeSequence;
//        SetCtrl sideLungeSet;
//
//        pauseExercise = new ExerciseCtrl(context.getString(R.string.rest), 60000, false);
//        pauseSequence = new SequenceCtrl(false, pauseExercise);
//        pauseExercise.setRestInterval(true);
//        pauseSet = new SetCtrl(pauseSequence, 1, true);
//        pauseSet.setCallback(workout);
//        pauseExercise.setCallback(pauseSequence);
//        pauseSequence.setCallback(pauseSet);
//
//        pushUp = new ExerciseCtrl(context.getString(R.string.push_ups), 1000, true);
//        pushUpSequence = new SequenceCtrl(true, pushUp, 45);
//        pushUpSet = new SetCtrl(context, pushUpSequence, 3, 15000, true);
//        pushUpSet.setCallback(workout);
//        pushUpSequence.setCallback(pushUpSet);
//        pushUp.setCallback(pushUpSequence);
//        allSets.add(pushUpSet);
//
//        allSets.add(pauseSet);
//
//        squat = new ExerciseCtrl(context.getString(R.string.squats), 1200, true);
//        squatSequence = new SequenceCtrl(true, squat, 38);
//        squatSet = new SetCtrl(context, squatSequence, 3, 15000, true);
//        squatSet.setCallback(workout);
//        squatSequence.setCallback(squatSet);
//        squat.setCallback(squatSequence);
//        allSets.add(squatSet);
//
//        allSets.add(pauseSet);
//
//        buttKick = new ExerciseCtrl(context.getString(R.string.butt_kicks_run), 600, true);
//        buttKickSequence = new SequenceCtrl(true, buttKick, 75);
//        buttKickSet = new SetCtrl(context, buttKickSequence, 3, 15000, true);
//        buttKickSet.setCallback(workout);
//        buttKickSequence.setCallback(buttKickSet);
//        buttKick.setCallback(buttKickSequence);
//        allSets.add(buttKickSet);
//
//        allSets.add(pauseSet);
//
//        tricepsDip = new ExerciseCtrl(context.getString(R.string.triceps_dips), 1200, true);
//        tricepsDipSequence = new SequenceCtrl(true, tricepsDip, 38);
//        tricepsDipSet = new SetCtrl(context, tricepsDipSequence, 3, 15000, true);
//        tricepsDipSet.setCallback(workout);
//        tricepsDipSequence.setCallback(tricepsDipSet);
//        tricepsDip.setCallback(tricepsDipSequence);
//        allSets.add(tricepsDipSet);
//
//        allSets.add(pauseSet);
//
//        sideLunge = new ExerciseCtrl(context.getString(R.string.side_lunges), context.getString(R.string.both_sides), 1750, true);
//        sideLungeSequence = new SequenceCtrl(true, sideLunge, 30);
//        sideLungeSet = new SetCtrl(context, sideLungeSequence, 3, 15000, true);
//        sideLungeSet.setCallback(workout);
//        sideLungeSequence.setCallback(sideLungeSet);
//        sideLunge.setCallback(sideLungeSequence);
//        allSets.add(sideLungeSet);
//
//        return allSets;
//    }
//
//    static private ArrayList<SetCtrl> getHighIntensityIntervalTraining30min(Context context, WorkoutCtrl workout) {
//        ArrayList<SetCtrl> allSets = new ArrayList<>();
//
//        ExerciseCtrl pauseExercise;
//        SequenceCtrl pauseSequence;
//        SetCtrl pauseSet;
//
//        ExerciseCtrl pushUp;
//        SequenceCtrl pushUpSequence;
//        SetCtrl pushUpSet;
//        ExerciseCtrl squat;
//        SequenceCtrl squatSequence;
//        SetCtrl squatSet;
//        ExerciseCtrl buttKick;
//        SequenceCtrl buttKickSequence;
//        SetCtrl buttKickSet;
//        ExerciseCtrl tricepsDip;
//        SequenceCtrl tricepsDipSequence;
//        SetCtrl tricepsDipSet;
//        ExerciseCtrl sideLunge;
//        SequenceCtrl sideLungeSequence;
//        SetCtrl sideLungeSet;
//        ExerciseCtrl jumpingJack;
//        SequenceCtrl jumpingJackSequence;
//        SetCtrl jumpingJackSet;
//        ExerciseCtrl sitUp;
//        SequenceCtrl sitUpSequence;
//        SetCtrl sitUpSet;
//
//        pauseExercise = new ExerciseCtrl(context.getString(R.string.rest), 60000, false);
//        pauseSequence = new SequenceCtrl(false, pauseExercise);
//        pauseExercise.setRestInterval(true);
//        pauseSet = new SetCtrl(pauseSequence, 1, true);
//        pauseSet.setCallback(workout);
//        pauseExercise.setCallback(pauseSequence);
//        pauseSequence.setCallback(pauseSet);
//
//        pushUp = new ExerciseCtrl(context.getString(R.string.push_ups), 1000, true);
//        pushUpSequence = new SequenceCtrl(true, pushUp, 45);
//        pushUpSet = new SetCtrl(context, pushUpSequence, 3, 15000, true);
//        pushUpSet.setCallback(workout);
//        pushUpSequence.setCallback(pushUpSet);
//        pushUp.setCallback(pushUpSequence);
//        allSets.add(pushUpSet);
//
//        allSets.add(pauseSet);
//
//        squat = new ExerciseCtrl(context.getString(R.string.squats), 1200, true);
//        squatSequence = new SequenceCtrl(true, squat, 38);
//        squatSet = new SetCtrl(context, squatSequence, 3, 15000, true);
//        squatSet.setCallback(workout);
//        squatSequence.setCallback(squatSet);
//        squat.setCallback(squatSequence);
//        allSets.add(squatSet);
//
//        allSets.add(pauseSet);
//
//        buttKick = new ExerciseCtrl(context.getString(R.string.butt_kicks_run), 600, true);
//        buttKickSequence = new SequenceCtrl(true, buttKick, 75);
//        buttKickSet = new SetCtrl(context, buttKickSequence, 3, 15000, true);
//        buttKickSet.setCallback(workout);
//        buttKickSequence.setCallback(buttKickSet);
//        buttKick.setCallback(buttKickSequence);
//        allSets.add(buttKickSet);
//
//        allSets.add(pauseSet);
//
//        tricepsDip = new ExerciseCtrl(context.getString(R.string.triceps_dips), 1200, true);
//        tricepsDipSequence = new SequenceCtrl(true, tricepsDip, 38);
//        tricepsDipSet = new SetCtrl(context, tricepsDipSequence, 3, 15000, true);
//        tricepsDipSet.setCallback(workout);
//        tricepsDipSequence.setCallback(tricepsDipSet);
//        tricepsDip.setCallback(tricepsDipSequence);
//        allSets.add(tricepsDipSet);
//
//        allSets.add(pauseSet);
//
//        sideLunge = new ExerciseCtrl(context.getString(R.string.side_lunges), context.getString(R.string.both_sides), 45000, true);
//        sideLungeSequence = new SequenceCtrl(true, sideLunge);
//        sideLungeSet = new SetCtrl(context, sideLungeSequence, 3, 15000, true);
//        sideLungeSet.setCallback(workout);
//        sideLungeSequence.setCallback(sideLungeSet);
//        sideLunge.setCallback(sideLungeSequence);
//        allSets.add(sideLungeSet);
//
//        allSets.add(pauseSet);
//
//        jumpingJack = new ExerciseCtrl(context.getString(R.string.jumping_jacks), 750, true);
//        jumpingJackSequence = new SequenceCtrl(true, jumpingJack, 54);
//        jumpingJackSet = new SetCtrl(context, jumpingJackSequence, 3, 15000, true);
//        jumpingJackSet.setCallback(workout);
//        jumpingJackSequence.setCallback(jumpingJackSet);
//        jumpingJack.setCallback(jumpingJackSequence);
//        allSets.add(jumpingJackSet);
//
//        allSets.add(pauseSet);
//
//        sitUp = new ExerciseCtrl(context.getString(R.string.sit_ups), 45000, true);
//        sitUpSequence = new SequenceCtrl(true, sitUp);
//        sitUpSet = new SetCtrl(context, sitUpSequence, 3, 15000, true);
//        sitUpSet.setCallback(workout);
//        sitUpSequence.setCallback(sitUpSet);
//        sitUp.setCallback(sitUpSequence);
//        allSets.add(sitUpSet);
//
//        return allSets;
//    }
}
