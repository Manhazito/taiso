package org.alphascorpii.taiso.controller;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.CountDownTimer;
import android.util.Log;

import org.alphascorpii.taiso.R;
import org.alphascorpii.taiso.application.TaisoApplication;
import org.alphascorpii.taiso.model.DaoSession;
import org.alphascorpii.taiso.model.Exercise;
import org.alphascorpii.taiso.model.ExerciseDao;
import org.alphascorpii.taiso.model.RootExercise;
import org.alphascorpii.taiso.model.Workout;
import org.alphascorpii.taiso.model.WorkoutDao;
import org.alphascorpii.taiso.model.WorkoutExercise;
import org.alphascorpii.taiso.model.WorkoutExerciseDao;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * [description]
 *
 * @author Filipe Ramos
 * @version 1.0
 */
public class WorkoutController {
    @SuppressWarnings("unused")
    private static final String TAG = WorkoutController.class.getSimpleName();

    private static final String SHARED_PREFS_KEY_JSON_VERSION = "SHARED_PREFS_KEY_JSON_VERSION";

    public static final int ALL_EXERCISES = -123;

    public static final int EXERCISE_TOKEN = 0;
    public static final int EXERCISE_PAUSE_TOKEN = 1;
    public static final int EXERCISE_SYMMETRY_PAUSE_TOKEN = 2;
    public static final int WORKOUT_PAUSE_TOKEN = 3;

    public static final int ORDER_BY_NAME = 0;
    public static final int ORDER_BY_LEVEL = 1;
    public static final int ORDER_BY_DURATION = 2;
    public static final int ORDER_ASC = 0;
    public static final int ORDER_DSC = 1;

    private static final int INVALID_ID = -1;

    private CountDownTimer countDownTimer;
    private boolean workoutStarted = false;
    private boolean workoutPaused = false;
    private long millisUntilFinished;
    private int token = EXERCISE_TOKEN;

    private TimerCallback callback;
    private Context context;
    private long duration;
    private SoundPool soundPool;
    private int soundId;

    public WorkoutController(Context context, TimerCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    @SuppressWarnings("deprecation")
    public void initSounds(Context context) {
        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        ((Activity) context).setVolumeControlStream(AudioManager.STREAM_MUSIC);
        soundId = soundPool.load(context, R.raw.beep, 1);
    }

    public void releaseSounds() {
        soundPool.release();
    }

    public void startTimer(int token, long duration) {
        this.token = token;
        startCountdown(duration);
        workoutStarted = true;
        workoutPaused = false;
    }

    public void startExercise(Exercise exercise) {
        token = EXERCISE_TOKEN;
        startCountdown(exercise.getDuration());
        workoutStarted = true;
        workoutPaused = false;
    }

    public void startExercisePause(Exercise exercise) {
        token = EXERCISE_PAUSE_TOKEN;
        startCountdown(exercise.getIntervalDuration());
        workoutStarted = true;
        workoutPaused = false;
    }

    public void startExerciseSymmetryPause(Exercise exercise) {
        token = EXERCISE_SYMMETRY_PAUSE_TOKEN;
        startCountdown(exercise.getIntervalDuration());
        workoutStarted = true;
        workoutPaused = false;
    }

    public void startWorkoutPause(Workout workout) {
        token = WORKOUT_PAUSE_TOKEN;
        startCountdown(workout.getIntervalDuration());
        workoutStarted = true;
        workoutPaused = false;
    }

    public void pauseWorkout() {
        if (countDownTimer != null) countDownTimer.cancel();
        workoutPaused = true;
    }

    public void resumeWorkout() {
        countDownTimer = new CountDownTimer(millisUntilFinished, 1L) {
            @Override
            public void onTick(long millisUntilFinished) {
                callback.timerTicked(token, millisUntilFinished, duration);
                WorkoutController.this.millisUntilFinished = millisUntilFinished;
            }

            @Override
            public void onFinish() {
                callback.timerTicked(token, 0L, duration);
                callback.timerEnded(token);
            }
        }.start();
        workoutPaused = false;
    }

    public boolean isWorkoutStarted() {
        return workoutStarted;
    }

    public boolean isWorkoutPaused() {
        return workoutPaused;
    }

    private void startCountdown(final long duration) {
        callback.timerStarted(token);
        this.duration = duration;

        countDownTimer = new CountDownTimer(duration, 10L) {
            @Override
            public void onTick(long millisUntilFinished) {
                callback.timerTicked(token, millisUntilFinished, duration);
                WorkoutController.this.millisUntilFinished = millisUntilFinished;
            }

            @Override
            public void onFinish() {
                callback.timerTicked(token, 0L, duration);
                callback.timerEnded(token);
            }
        }.start();
    }

    public void playShortBeep() {
        soundPool.play(soundId, 1, 1, 1, 0, 2.0f);
    }

    public void playShortBeepBeep() {
        soundPool.play(soundId, 1, 1, 1, 1, 1.8f);
    }

    public void playShortBeepBeepBeep() {
        soundPool.play(soundId, 1, 1, 1, 2, 1.6f);
    }

    public void playLongBeep() {
        soundPool.play(soundId, 1, 1, 1, 4, 1.2f);
    }

    public int getStringResource(String name) {
        return context.getResources().getIdentifier(name, "string", context.getPackageName());
    }

    public int getDrawableResource(String name) {
        return context.getResources().getIdentifier(name, "drawable", context.getPackageName());
    }

    public long getMillisUntilFinished() {
        return millisUntilFinished;
    }

    public ArrayList<String> getWorkoutGroups(Context context) {
        ArrayList<String> workoutGroups = new ArrayList<>();

        DaoSession session = ((TaisoApplication) context.getApplicationContext()).getDaoSession();
        if (session != null) {
            for (Workout workout : session.getWorkoutDao().loadAll()) {
                String testGroup = workout.getGroup();
                boolean newGroup = true;
                for (String group : workoutGroups) {
                    if (group.equals(testGroup)) {
                        newGroup = false;
                    }
                }
                if (newGroup) workoutGroups.add(testGroup);
            }
        }

        return workoutGroups;
    }

    public ArrayList<Workout> getWorkoutsFromGroup(String group) {
        DaoSession session = ((TaisoApplication) context.getApplicationContext()).getDaoSession();
        return new ArrayList<>(session.getWorkoutDao().queryBuilder().where(WorkoutDao.Properties.Group.eq(group)).list());
    }

    public long getWorkoutDuration(Workout workout) {
        long workoutDuration = 0L;
        DaoSession session = ((TaisoApplication) context.getApplicationContext()).getDaoSession();

        ArrayList<WorkoutExercise> workoutExercises = new ArrayList<>(session.getWorkoutExerciseDao().queryBuilder().where(WorkoutExerciseDao.Properties.WorkoutId.eq(workout.getId())).list());
        for (WorkoutExercise workoutExercise : workoutExercises) {
            Exercise exercise = session.getExerciseDao().queryBuilder().where(ExerciseDao.Properties.Id.eq(workoutExercise.getExerciseId())).list().get(0);
            workoutDuration += exercise.getDuration() * exercise.getRepetitions() * exercise.getSets();
            workoutDuration += exercise.getIntervalDuration() * (exercise.getSets() - 1);

            if (!exercise.getRootExercise().getIsSymmetrical()) {
                workoutDuration *= 2;
                workoutDuration += exercise.getIntervalDuration();
            }
        }
        workoutDuration += workout.getIntervalDuration() * (workoutExercises.size() - 1);

        return workoutDuration;
    }

    public long getExerciseDuration(Exercise exercise) {
        long exerciseDuration = 0L;

        exerciseDuration += exercise.getDuration() * exercise.getRepetitions() * exercise.getSets();

        if (!exercise.getRootExercise().getIsSymmetrical()) {
            exerciseDuration *= 2;
            exerciseDuration += exercise.getIntervalDuration();
        }

        return exerciseDuration;
    }

    public ArrayList<Exercise> getExercises(Context context, long workoutId) {
        DaoSession session = ((TaisoApplication) context.getApplicationContext()).getDaoSession();
        ArrayList<Exercise> exercises = new ArrayList<>();

        if (workoutId == ALL_EXERCISES) {
            exercises = new ArrayList<>(session.getExerciseDao().queryBuilder().orderAsc(ExerciseDao.Properties.RootExerciseId).list());
        } else {
            ArrayList<WorkoutExercise> workoutExercises = new ArrayList<>(session.getWorkoutExerciseDao().queryBuilder().where(WorkoutExerciseDao.Properties.WorkoutId.eq(workoutId)).orderAsc(WorkoutExerciseDao.Properties.PositionInWorkout).list());
            for (WorkoutExercise workoutExercise : workoutExercises) {
                exercises.add(session.getExerciseDao().load(workoutExercise.getExerciseId()));
            }
        }

        return exercises;
    }

    public ArrayList<RootExercise> getRootExercises(Context context, long workoutId) {
        DaoSession session = ((TaisoApplication) context.getApplicationContext()).getDaoSession();
        ArrayList<RootExercise> rootExercises = new ArrayList<>();
        ArrayList<Exercise> exercises = getExercises(context, workoutId);

        for (Exercise exercise : exercises) {
            if (!rootExercises.contains(session.getRootExerciseDao().load(exercise.getRootExerciseId()))) {
                rootExercises.add(session.getRootExerciseDao().load(exercise.getRootExerciseId()));
            }
        }

        return rootExercises;
    }

    public ArrayList<Exercise> sortExercises(ArrayList<Exercise> exercises, final int orderType, final int orderDirection) {
        ArrayList<Exercise> result = new ArrayList<>(exercises);
        Collections.sort(result, new Comparator<Exercise>() {
            @Override
            public int compare(Exercise exercise1, Exercise exercise2) {
                int compareValue = 0;
                switch (orderType) {
                    case ORDER_BY_NAME:
                        String exercise1Name = context.getResources().getString(getStringResource(exercise1.getRootExercise().getName())).toLowerCase();
                        String exercise2Name = context.getResources().getString(getStringResource(exercise2.getRootExercise().getName())).toLowerCase();
                        compareValue = exercise1Name.compareTo(exercise2Name);

                        if (orderDirection == ORDER_DSC) {
                            compareValue *= -1;
                        }
                        break;

                    case ORDER_BY_DURATION:
                        Long exercise1duration = getExerciseDuration(exercise1);
                        Long exercise2duration = getExerciseDuration(exercise2);
                        compareValue = exercise1duration.compareTo(exercise2duration);

                        if (orderDirection == ORDER_DSC) {
                            compareValue *= -1;
                        }
                        break;
                }

                return compareValue;
            }
        });

        return result;
    }

    public ArrayList<Workout> sortWorkouts(ArrayList<Workout> workouts, final int orderType, final int orderDirection) {
        ArrayList<Workout> result = new ArrayList<>(workouts);
        Collections.sort(result, new Comparator<Workout>() {
            @Override
            public int compare(Workout workout1, Workout workout2) {
                int compareValue = 0;
                switch (orderType) {
                    case ORDER_BY_NAME:
                        String workout1Name = context.getResources().getString(getStringResource(workout1.getName())).toLowerCase();
                        String workout2Name = context.getResources().getString(getStringResource(workout2.getName())).toLowerCase();
                        compareValue = workout1Name.compareTo(workout2Name);

                        if (orderDirection == ORDER_DSC) {
                            compareValue *= -1;
                        }
                        break;

                    case ORDER_BY_LEVEL:
                        compareValue = workout1.getLevel().compareTo(workout2.getLevel());

                        if (orderDirection == ORDER_DSC) {
                            compareValue *= -1;
                        }
                        break;

                    case ORDER_BY_DURATION:
                        Long workout1duration = getWorkoutDuration(workout1);
                        Long workout2duration = getWorkoutDuration(workout2);
                        compareValue = workout1duration.compareTo(workout2duration);

                        if (orderDirection == ORDER_DSC) {
                            compareValue *= -1;
                        }
                        break;
                }

                return compareValue;
            }
        });

        return result;
    }

    public void loadJsonWorkouts(JsonLoaderCallback callback, DaoSession session) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(TaisoApplication.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
        int myJsonVersion = sharedPreferences.getInt(SHARED_PREFS_KEY_JSON_VERSION, -1);
        myJsonVersion = -1; //TODO: REMOVE!!!
        StringBuilder jsonData = new StringBuilder();

        try {
            InputStream rawData = context.getResources().openRawResource(R.raw.workouts);
            BufferedReader reader = new BufferedReader(new InputStreamReader(rawData));
            String line;

            while ((line = reader.readLine()) != null) {
                jsonData.append(line);
            }

            JSONObject jsonObject = new JSONObject(jsonData.toString());

            int jsonVersion = jsonObject.getInt("json_version");

            Log.d(TAG, "My JSON version: " + myJsonVersion + " | loaded JSON version: " + jsonVersion);
            if (myJsonVersion < jsonVersion) {
                Log.d(TAG, "Recreating database!");
                JSONArray jsonArray = (new JSONObject(jsonData.toString())).getJSONArray("workouts");

                // Must have another db for user exercises and workouts.
                // If user creates a exercise with one of these root exercises it must be copied to the user db,
                // because in a future update it may be removed from the original db.
                session.getWorkoutDao().deleteAll();
                session.getExerciseDao().deleteAll();
                session.getWorkoutExerciseDao().deleteAll();
                session.getRootExerciseDao().deleteAll();

                for (int i = 0; i < jsonArray.length(); ++i) {
                    JSONObject workoutObject = jsonArray.getJSONObject(i);
                    parseWorkout(session, workoutObject);
                }
                sharedPreferences.edit().putInt(SHARED_PREFS_KEY_JSON_VERSION, jsonVersion).apply();
                Log.d(TAG, "Database recreated.");
            }
            callback.finishedLoading();
        } catch (IOException | JSONException e) {
            e.printStackTrace(); //TODO: Dialog -> "If the problem persists, reinstall the app."
            Log.e(TAG, e.getMessage());
        }
    }

    private void parseWorkout(DaoSession session, JSONObject workoutObject) throws JSONException {
        Workout workout = new Workout();
        long workoutId;

        workout.setName(workoutObject.getString("name"));
        workout.setTitle(workoutObject.getString("title"));
        workout.setDetails(workoutObject.getString("details"));
        workout.setGroup(workoutObject.getString("group"));
        workout.setLevel(workoutObject.getInt("level"));
        workout.setIntervalDuration(workoutObject.getLong("intervalDuration"));

        JSONArray exercisesArray = workoutObject.getJSONArray("exercises");
        int numberOfExercises = exercisesArray.length();

        workoutId = session.getWorkoutDao().insert(workout);

        for (int i = 0; i < numberOfExercises; i++) {
            JSONObject exerciseObject = exercisesArray.getJSONObject(i);
            parseExercise(session, workoutId, exerciseObject, i + 1);
        }
    }

    private void parseExercise(DaoSession session, long workoutId, JSONObject exerciseObject, int position) throws JSONException {
        Exercise exercise = new Exercise();

        JSONObject rootExerciseObject = exerciseObject.getJSONObject("rootExercise");

        long rootExerciseId = parseRootExercise(session, rootExerciseObject);
        long exerciseId;

        if (rootExerciseId != INVALID_ID) {
            exercise.setRootExerciseId(rootExerciseId);
            exercise.setDuration(exerciseObject.getLong("duration"));
            exercise.setWeightOnArms(exerciseObject.getInt("weightOnArms"));
            exercise.setWeightOnLegs(exerciseObject.getInt("weightOnLegs"));
            exercise.setWeightOnChest(exerciseObject.getInt("weightOnChest"));
            exercise.setWeightOnAbdomen(exerciseObject.getInt("weightOnAbdomen"));
            exercise.setRepetitions(exerciseObject.getInt("repetitions"));
            exercise.setSets(exerciseObject.getInt("sets"));
            exercise.setIntervalDuration(exerciseObject.getLong("intervalDuration"));

            exerciseId = session.getExerciseDao().insert(exercise);

            WorkoutExercise workoutExercise = new WorkoutExercise();
            workoutExercise.setPositionInWorkout(position);
            workoutExercise.setExerciseId(exerciseId);
            workoutExercise.setWorkoutId(workoutId);
            session.getWorkoutExerciseDao().insert(workoutExercise);
        } else {
            throw new JSONException("Invalid Root Exercise ID!");
        }
    }

    private long parseRootExercise(DaoSession session, JSONObject rootExerciseObject) throws JSONException {
        RootExercise rootExercise = new RootExercise();
        long rootExerciseId;

        rootExercise.setName(rootExerciseObject.getString("name"));
        rootExercise.setBase(rootExerciseObject.getString("base"));
        rootExercise.setIsSymmetrical(rootExerciseObject.getBoolean("isSymmetrical"));
        rootExercise.setCanUseWeightsOnArms(rootExerciseObject.getBoolean("canUseWeightsOnArms"));
        rootExercise.setCanUseWeightsOnLegs(rootExerciseObject.getBoolean("canUseWeightsOnLegs"));
        rootExercise.setCanUseWeightsOnChest(rootExerciseObject.getBoolean("canUseWeightsOnChest"));
        rootExercise.setCanUseWeightsOnAbdomen(rootExerciseObject.getBoolean("canUseWeightsOnAbdomen"));

        rootExerciseId = getRootExerciseId(session, rootExercise);
        if (rootExerciseId == INVALID_ID) {
            rootExerciseId = session.getRootExerciseDao().insert(rootExercise);
        }

        return rootExerciseId;
    }

    private long getRootExerciseId(DaoSession session, RootExercise rootExercise) {
        ArrayList<RootExercise> rootExerciseArrayList = new ArrayList<>(session.getRootExerciseDao().queryBuilder().list());

        if (rootExerciseArrayList.size() < 1) return INVALID_ID;

        for (RootExercise rootExerciseInDatabase : rootExerciseArrayList) {
            if (rootExerciseInDatabase.getName().equals(rootExercise.getName()) &&
                    rootExerciseInDatabase.getBase().equals(rootExercise.getBase()) &&
                    rootExerciseInDatabase.getIsSymmetrical().equals(rootExercise.getIsSymmetrical()) &&
                    rootExerciseInDatabase.getCanUseWeightsOnArms().equals(rootExercise.getCanUseWeightsOnArms()) &&
                    rootExerciseInDatabase.getCanUseWeightsOnLegs().equals(rootExercise.getCanUseWeightsOnLegs()) &&
                    rootExerciseInDatabase.getCanUseWeightsOnChest().equals(rootExercise.getCanUseWeightsOnChest()) &&
                    rootExerciseInDatabase.getCanUseWeightsOnAbdomen().equals(rootExercise.getCanUseWeightsOnAbdomen())
                    ) {
                return rootExerciseInDatabase.getId();
            }
        }

        return INVALID_ID;
    }

    public static void printRootExercise(RootExercise rootExercise) {
        Log.d(TAG, "\nName: " + rootExercise.getName());
        Log.d(TAG, "Base: " + rootExercise.getBase());
        Log.d(TAG, "Is symmetrical: " + rootExercise.getIsSymmetrical());
        Log.d(TAG, "Can use weight on arms: " + rootExercise.getCanUseWeightsOnArms());
        Log.d(TAG, "Can use weight on legs: " + rootExercise.getCanUseWeightsOnLegs());
        Log.d(TAG, "Can use weight on chest: " + rootExercise.getCanUseWeightsOnChest());
        Log.d(TAG, "Can use weight on abdomen: " + rootExercise.getCanUseWeightsOnAbdomen());
    }

    public static void printExercise(Exercise exercise) {
        printRootExercise(exercise.getRootExercise());
        Log.d(TAG, "\nDuration: " + exercise.getDuration());
        Log.d(TAG, "Repetitions: " + exercise.getRepetitions());
        Log.d(TAG, "Sets: " + exercise.getSets());
        Log.d(TAG, "Weight on arms: " + exercise.getWeightOnArms());
        Log.d(TAG, "Weight on legs: " + exercise.getWeightOnLegs());
        Log.d(TAG, "Weight on chest: " + exercise.getWeightOnChest());
        Log.d(TAG, "Weight on abdomen: " + exercise.getWeightOnAbdomen());
        Log.d(TAG, "Interval duration: " + exercise.getIntervalDuration());
    }

    public interface TimerCallback {
        void timerStarted(int token);

        void timerTicked(int token, long millisUntilFinished, long millisTotal);

        void timerEnded(int token);
    }

    public interface JsonLoaderCallback {
        void finishedLoading();
    }

}
