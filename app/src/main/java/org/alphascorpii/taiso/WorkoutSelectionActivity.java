package org.alphascorpii.taiso;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import org.alphascorpii.taiso.adapter.WorkoutItemAdapter;
import org.alphascorpii.taiso.controller.WorkoutController;
import org.alphascorpii.taiso.util.WorkoutFactory;

public class WorkoutSelectionActivity extends Activity implements WorkoutItemAdapter.WorkoutItemListener {
    @SuppressWarnings("unused")
    private static final String TAG = WorkoutSelectionActivity.class.getCanonicalName();

    WorkoutItemAdapter workoutItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); // No need for different layout…

        RecyclerView workoutTypesRecyclerView = (RecyclerView) findViewById(R.id.multiPurposeList);
        workoutTypesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        workoutTypesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        workoutItemAdapter = new WorkoutItemAdapter(this, getIntent().getStringExtra(WorkoutFactory.WORKOUT_GROUP_ID), this);
        workoutTypesRecyclerView.setAdapter(workoutItemAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_workout_selection_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort_by_name:
                workoutItemAdapter.sortWorkouts(WorkoutController.ORDER_BY_NAME);
                return true;
            case R.id.action_sort_by_duration:
                workoutItemAdapter.sortWorkouts(WorkoutController.ORDER_BY_DURATION);
                return true;
            case R.id.action_sort_by_level:
                workoutItemAdapter.sortWorkouts(WorkoutController.ORDER_BY_LEVEL);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(long workoutId) {
        Intent intent = new Intent(getBaseContext(), WorkoutPreviewActivity.class);
        intent.putExtra(WorkoutFactory.WORKOUT_ID, workoutId);
        startActivity(intent);
    }
}
