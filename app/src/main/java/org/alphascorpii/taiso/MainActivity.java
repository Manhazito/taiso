package org.alphascorpii.taiso;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.widget.Toast;

import org.alphascorpii.taiso.adapter.WorkoutGroupItemAdapter;
import org.alphascorpii.taiso.util.WorkoutFactory;

public class MainActivity extends Activity implements WorkoutGroupItemAdapter.WorkoutGroupItemListener {
    @SuppressWarnings("unused")
    private static final String TAG = MainActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = getTheme();
            theme.resolveAttribute(android.R.attr.colorPrimary, typedValue, true);
            int color = typedValue.data;

            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_overview_screen);
            ActivityManager.TaskDescription td = new ActivityManager.TaskDescription(null, bm, color);

            setTaskDescription(td);
            bm.recycle();
        }

        RecyclerView workoutTypes = (RecyclerView) findViewById(R.id.multiPurposeList);
        workoutTypes.setLayoutManager(new LinearLayoutManager(this));
        workoutTypes.setItemAnimator(new DefaultItemAnimator());
        workoutTypes.setAdapter(new WorkoutGroupItemAdapter(this, this));
    }

    @Override
    public void onItemClick(String workoutGroupIdentifier) {
        switch (workoutGroupIdentifier) {
            case WorkoutGroupItemAdapter.CUSTOM_WORKOUTS_IDENTIFIER:
//                Toast.makeText(this, "Not implemented yet…", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getBaseContext(), RootExerciseListActivity.class));
                break;
            case WorkoutGroupItemAdapter.RANDOM_WORKOUTS_IDENTIFIER:
                Toast.makeText(this, "Not implemented yet…", Toast.LENGTH_SHORT).show();
                break;
            default:
                Intent intent = new Intent(getBaseContext(), WorkoutSelectionActivity.class);
                intent.putExtra(WorkoutFactory.WORKOUT_GROUP_ID, workoutGroupIdentifier);
                startActivity(intent);
                break;
        }
    }
}
