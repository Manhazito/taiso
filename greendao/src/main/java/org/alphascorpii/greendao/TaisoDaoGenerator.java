package org.alphascorpii.greendao;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class TaisoDaoGenerator {
    private static final int privateDbVersion = 1;

    public static void main(String args[]) throws Exception {
        // Private DB
        Schema privateSchema = new Schema(privateDbVersion, "org.alphascorpii.taiso.model");

        // Root Exercise
        Entity rootExercise = privateSchema.addEntity("RootExercise");
        rootExercise.addIdProperty();
        rootExercise.addStringProperty("name").notNull(); // Use to load ImageResourceId and StringResourceId (title, sub-title and description) using getIdentifier()
        rootExercise.addStringProperty("base");
        rootExercise.addBooleanProperty("isSymmetrical");
        rootExercise.addBooleanProperty("canUseWeightsOnArms");
        rootExercise.addBooleanProperty("canUseWeightsOnLegs");
        rootExercise.addBooleanProperty("canUseWeightsOnChest");
        rootExercise.addBooleanProperty("canUseWeightsOnAbdomen");

        // Exercise
        Entity exercise = privateSchema.addEntity("Exercise");
        exercise.addIdProperty();
        Property rootExerciseId = exercise.addLongProperty("rootExerciseId").notNull().getProperty();
        exercise.addToOne(rootExercise, rootExerciseId).setName("rootExercise");
        exercise.addLongProperty("duration");
        exercise.addIntProperty("weightOnArms"); // In grams
        exercise.addIntProperty("weightOnLegs"); // In grams
        exercise.addIntProperty("weightOnChest"); // In grams
        exercise.addIntProperty("weightOnAbdomen"); // In grams
        exercise.addIntProperty("repetitions");
        exercise.addIntProperty("sets");
        exercise.addLongProperty("intervalDuration");

        // Workout
        Entity workout = privateSchema.addEntity("Workout");
        workout.addIdProperty();
        workout.addStringProperty("name").notNull().unique(); // Use to load StringResourceId (title and details) using getIdentifier() — if not provided!
        workout.addStringProperty("title");
        workout.addStringProperty("details");
        workout.addStringProperty("group");
        workout.addIntProperty("level"); // In a group, represents the level; If no group, represents the easiness.
        workout.addLongProperty("intervalDuration"); // …between sets.


        // WorkoutExercise
        Entity workoutExercise = privateSchema.addEntity("WorkoutExercise");
        Property workoutId = workoutExercise.addLongProperty("workoutId").notNull().getProperty();
        workoutExercise.addToOne(workout, workoutId).setName("workout");
        Property exerciseId = workoutExercise.addLongProperty("exerciseId").notNull().getProperty();
        workoutExercise.addToOne(exercise, exerciseId).setName("exercise");
        workoutExercise.addIntProperty("positionInWorkout");


        new DaoGenerator().generateAll(privateSchema, "app/src/main/java");
    }
}
